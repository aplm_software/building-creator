from reqs import *

class vertex:
	def __init__(self,x,y,z,index,parent_type,parent_index):
		self.x=float(x)
		self.y=float(y)
		self.z=float(z)
		self.index=int(index) #index relative to whatever the front is...
		self.reset()

		self.parent_type=parent_type #type of the parent (ie. feature, front)

		self.parent_index=parent_index #index of the parent (that exists in the super class)

	def return_x(self):
		return(self.x)
	def return_y(self):
		return(self.y)
	def return_z(self):
		return(self.z)

	def reset(self):
		self.vert = [float(self.x),float(self.y),float(self.z)]
		self.form_string()
		return self

	def form_string(self):
		self.vertex_str = 'v ' + '{:<08f}'.format(self.x) + ' ' + '{:<08f}'.format(self.y) + ' ' + '{:<08f}'.format(self.z)
		return self

	def reset_index(self,new_index):
		self.index=new_index
		return self

	def shift_index_up(self,shift_amt):
		self.index+=shift_amt
		return self

	def shift_index_down(self,shift_amt):
		self.index-=shift_amt
		return self		

	def ret_axis(self,index):
		try:
			if index in ['0',0,'x']:
				return self.return_x()
			if index in ['1',1,'y']:
				return self.return_y()
			if index in ['2',2,'z']:
				return self.return_z()
		except ValueError: 
			print('Index: ' + str(index) + ' not applicable for axis return function for vertex')


	def change_all_axes_absolute(self,ax_str,new_values):
		self.x,self.y,self.z=new_values
		self.reset()
		return self

	def change_all_axes_relative(self,amts):
		#change all vertex values relatively (by adding)
		#set x,y,z
		self.x += amts[0]
		self.y += amts[1]
		self.z += amts[2]
		self.reset()
		return self

	def change_axis_rel(self,index,change_amt):
		try:

			if index in ['0',0,'x']:
				self.x+=change_amt
				self.reset()
				return self

			if index in ['1',1,'y']:
				self.y+=change_amt
				self.reset()
				return self

			if index in ['2',2,'z']:
				self.z+=change_amt
				self.reset()
				return self

		except ValueError: 
			print('Index: ' + str(index) + ' not applicable for axis return function for vertex')


	def change_x_rel(self,change_amt):
		self.x+=change_amt
		self.reset()
		return self

	def change_y_rel(self,change_amt):
		self.x+=change_amt
		self.reset()
		return self

	def change_z_rel(self,change_amt):
		self.x+=change_amt
		self.reset()
		return self

	def scale_axis(self,index,scale_amt):
		try:
			if index in ['0',0,'x']:
				self.x = self.x * float(scale_amt)
				self.reset()
				return self

			if index in ['1',1,'y']:
				self.y = self.y * float(scale_amt)
				self.reset()
				return self

			if index in ['2',2,'z']:
				self.z = self.z * float(scale_amt)
				self.reset()
				return self

		except ValueError: 
			print('Index: ' + str(index) + ' not applicable for axis return function for vertex')


	def change_all_axes_scale(self,scale_amts):
		self.x = self.x * float(scale_amts[0])
		self.y = self.y * float(scale_amts[1])
		self.z = self.z * float(scale_amts[2])
		self.reset()
		return self		

	def change_v_index(self,amt):
		#increment the index by amt...
		self.index+=amt
		self.reset()
		return self

	def get_np_v(self):
		return np.array([self.x, self.y, self.z])

	def get_vertex_vector(self):
		return [self.x,self.y,self.z]

	def rotate_angles(self, angles):
		rotation_matrix = return_rotation_matrix_3d(angles)
		self.rotate_3d_matrix(rotation_matrix)
		return self

	def rotate_3d_matrix(self, rotation_matrix):
		rotation_matrix_in = rotation_matrix.astype(float)
		vertex_points_in = self.get_np_v().astype(float)
		self.x,self.y,self.z = np.matmul(rotation_matrix_in,vertex_points_in)
		self.reset()
		return self