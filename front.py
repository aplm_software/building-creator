from reqs import *


class front:

	# The front holds the faces and vertices etc etc...

	def __init__(self, dims):
	    # dims is a list of min_min etc points

	    self.vertices = return_four_front_face_vertices(dims)

	    self.min_min_v_i = 1
	    self.min_max_v_i = 2
	    self.max_max_v_i = 3
	    self.max_min_v_i = 4

	    self.normal_vectors = return_default_normal_vectors()

	    # must be the case to start off with
	    self.front_normal_vector_index = DEFAULT_NORMAL_VECTOR_INDEX

	    for nv in self.normal_vectors:
	    	if nv.is_same(DEFAULT_NORMAL_VEC):
	    		self.front_normal_vector=nv

	    #start off with one face
	    self.faces = [return_face(self.vertices, self.front_normal_vector_index, self.front_normal_vector.vec)]

	    self.cut_holes = []  # empty initially, but as we make more cuts
	    					# this thing grows in size etc
	    # front stores the indicies associated w each front
	    # also store vertic/fac associated w them

	    self.features = [] #these features have been added to the front and have x,y,z vals

	    self.reset_face_normal_vector_vectors() #instantiate normal vector vec values!

	def return_front_vertex_attributes(self):
		self.xs = []
		self.ys = []
		self.zs = []
		for v in self.vertices:
			self.xs.append(v.x)
			self.ys.append(v.y)
			self.zs.append(v.z)

		self.maxs = [max(self.xs), max(self.ys), max(self.zs)]
		self.mins = [min(self.xs), min(self.ys), min(self.zs)]

		self.mps = [return_mp(self.xs),
					return_mp(self.ys),
					return_mp(self.zs)]


		return self


	def add_front(self,in_front):
		#to be used when we are ready to print and go to output!

		#compare and update vn's



		#add vertices into the fold

		tot_verts = len(self.vertices)


		in_front.increment_all_front_vertex_and_face_indices(tot_verts)

		#align vn's?

		#vertices and faces add into them...
		self.vertices+=in_front.vertices
		self.faces+=in_front.faces

		return self



	def rotate_front_to_default_norm(self):

		# Rotate self.orig_vn to BASELINE_NORM, then rel_ax_ind will be 0
		default_n_inv_rot = inverse_rotation_matrix(DEFAULT_NORMAL_VEC, self.front_normal_vector.vec)

		self.rotate_front_3dmat(default_n_inv_rot)

		return self



	def update_front_faces_vertex_coordinates(self):
		#called any time that we modify something (like rotate, translate)
		for f in self.faces:
			current_f_coordinates=[]
			#make sure don't mess up the order! important!
			relevant_v_is = f.face_vertex_indices
			for relevant_v_i in relevant_v_is:
				current_f_coordinates.append(self.vertices[relevant_v_i-1].get_vertex_vector())

			f.update_vertex_coordinates(current_f_coordinates)

		return self


	def rotate_front_3dmat(self, rotation_matrix):


	    # rotations can either be a rotation matrix (numpy mat) OR a list of rotations
	    # on each axis eg [90,90,90]
	    # need to autodetect using if isinstance etc...
	    self.find_front_mp()  # gets mps

	    self.center_front_vertices()  # center around mid point
	    # rotate them, including updating vn's


	    self.rotate_front_vertices_3dmat(rotation_matrix)
	    self.uncenter_front_vertices()  # uncenter them
	    self.update_front_faces_vertex_coordinates()


	    #update other vectors etc...


	    return self

	def unrotate_from_default_norm(self,target_normal_vector):

	    # Restores original rotation of front!

	    # Not going to work cos front_normal_vector is overwritten...


	    default_n_inv_rot = inverse_rotation_matrix(
	                        target_normal_vector,
	                        DEFAULT_NORMAL_VEC)

	    self.rotate_front_3dmat(default_n_inv_rot)

	    return self

	def find_front_mp(self):
	    self.return_front_vertex_attributes()  # helper method, deprecated
	    return self

	def translate_front(self, change_amt):
		self.translate_front_vertices(change_amt)
		return self

	def translate_front_vertices(self, change_amt):
		for v in self.vertices:
			#print(v)
			v.change_all_axes_relative(change_amt)

		self.update_front_faces_vertex_coordinates() #update faces
		return self

	def cut_hole_in_front(self, hole):
		return self

	def center_front(self):
		self.center_front_vertices()
		return self

	def center_front_vertices(self):
	    change_amt = [-m for m in self.mps]
	    self.translate_front_vertices(change_amt)
	    return self

	def uncenter_front_vertices(self):
	    self.translate_front_vertices(self.mps)
	    return self

	def rotate_front_vertices_3dmat(self, rotation_matrix):
		for v in self.vertices:
			v.rotate_3d_matrix(rotation_matrix)

		# Rotate all vertices around

		self.recast_normal_vectors_3dmat(rotation_matrix) 

		#1. finds bijection of new mapping for vn's
		#2. updates normal_vectors to include ALL vn's

		self.renumber_front_vn_references_in_faces() 

		# Updates the references to vn in each face
		# Then clear the bijection so it's empty and we don't do it twice 

		self.update_front_normal_vector_after_rotation_3dmat(rotation_matrix)

		# update the front's normal vector
		self.update_front_normal_vector_index()

		# update the front's normal vector index
		self.reset_face_normal_vector_vectors() 
		# return the vector values of the normal vectors

		return self

	def reset_face_normal_vector_vectors(self):

		for f in self.faces:
			f.normal_vector_vec = self.normal_vectors[f.normal_vec_index-1].vec

		return self


	def reset_front_normal_vector(self):

		for normal_vector in self.normal_vectors:
			if normal_vector.is_same(new_feature_normal_vector_vec):
				NEW_NORMAL_VEC_FOUND=True
				self.front_normal_vector = normal_vector

				return self

		if NEW_NORMAL_VEC_FOUND==False:
			print('errror no new feature_normal_vector found!!!')

		return self

	def update_front_normal_vector_after_rotation_3dmat(self,rotation_matrix):
		#wherein we set front_normal_vector...

		old_feature_normal_vector = copy.deepcopy(self.front_normal_vector)

		new_feature_normal_vector_vec = old_feature_normal_vector.rotate_3dmat(rotation_matrix) #returns a vector

		NEW_NORMAL_VEC_FOUND=False
		for normal_vector in self.normal_vectors:
			if normal_vector.is_same(new_feature_normal_vector_vec):
				NEW_NORMAL_VEC_FOUND=True
				self.front_normal_vector = normal_vector

				return self

		if NEW_NORMAL_VEC_FOUND==False:
			print('errror no new feature_normal_vector found!!!')



		return self


	def update_front_normal_vector_index(self):
		self.front_normal_vector_index=self.front_normal_vector.index - 1
		return self

	def recast_normal_vectors(self, new_vn_vecs):
		vn_map_additions = []
		for new_vn_vec in new_vn_vecs:
			current_addition = None  # null in this instance
			for feature_vn in self.normal_vectors:
				if feature_vn.is_same(new_vn_vec):
					current_addition = feature_vn.index
			if current_addition == None:
				# find index of new vector
				new_vn_index = len(self.normal_vectors) + 1
				# create new vector
				self.normal_vectors.append(normal_vec(new_vn_vec, new_vn_index))
				# remap vn
				vn_map_additions.append(new_vn_index)

			else:
				vn_map_additions.append(current_addition + 1) #the index
		
		vn_map_additions = [vn for vn in vn_map_additions]
		vn_mp = list(np.argsort(vn_map_additions))
		vn_mp = [vn + 1 for vn in vn_mp]  # need to srt them...

		self.vn_map_additions = vn_mp  # for use when we remap the faces

		return self

	def recast_normal_vectors_3dmat(self, rotation_matrix):
		# new face will have to have its references to them updated
		new_vn_vecs = []
		for vn in self.normal_vectors:
			new_vn_vecs.append(vn.rotate_3dmat(rotation_matrix))

		self.recast_normal_vectors(new_vn_vecs)

		return self


	def renumber_front_vn_references_in_faces(self):
		# this recasts v normal reference in every face object (including the
		# string...)
		for f in self.faces:
			f.update_vn_ref(self.vn_map_additions)


		self.vn_map_additions=[]  # clear out so we don't iterate twice!
		return self

	def renumber_front_vn_references_to_building(self, building_concordance):
		# this recasts v normal reference in every face object (including the
		# string...)
		for f in self.faces:
			f.update_vn_ref(building_concordance)

		self.reset_front_normal_vector()  # reset the normal axis vector

		return self

	def increment_all_front_vertex_and_face_indices(self,increment_amt):

		#update vertices

		for v in self.vertices:
			v.shift_index_up(increment_amt)

		#update faces

		for f in self.faces:
			f.shift_all_face_vertex_references_up(increment_amt)

		return self

	def rotate_front_rotation_angles(self, rotation_angles):

		rotation_matrix=return_rotation_matrix_3d(rotation_angles)

		self.rotate_front_3dmat(rotation_matrix)

		return self

	def return_extreme_points(self):




		self.min_min_v = self.vertices[self.min_min_v_i-1]
		self.min_max_v = self.vertices[self.min_max_v_i-1]
		self.max_max_v = self.vertices[self.max_max_v_i-1]
		self.max_min_v = self.vertices[self.max_min_v_i-1]

		return self

	def scale_front(self, scale_amts):

		self.return_front_vertex_attributes()  # will get mp etc....

		self.center_front_vertices()  # need to scale this hong too

		for v in self.vertices:
			v.change_all_axes_scale(scale_amts)

		self.uncenter_front_vertices()  # uncenter

		return self

	# functions concern w append to vertices or faces
	def add_vertex(self, vert):
		self.vertices.append(vert)
		return self

	def add_face(self, vertices, normal_vec):
		self.faces.append(face(vertices, normal_vec, self.vertices))
		return self

	def create_face(self, vertex_indices):

	    # find relevant vertices
	    # returns the relevant vertices in question...
	    relevant_vertices = [self.vertices[v_i - 1] for v_i in vertex_indices]

	    # find coordinates of relevant vertices
	    relevant_v_coordinates = []

	    for relevant_v in relevant_vertices:
	        relevant_v_coordinates.append(v.get_vertex_vector)

	    # define a plane equation to get the normal vector
	    cur_face_eqn = return_plane_equation(relevant_v_coordinates)

	    cur_face_normal_vec_coeff = return_plane_coefficients(cur_face_eqn)

	    NV_FOUND = False
	    for nv in self.normal_vectors:
	        if nv.is_same(cur_face_normal_vec_coeff) and NV_FOUND == False:
	            cur_face_normal_vec = normal_vec(
	                cur_face_normal_vec_coeff, nv.index)
	            NV_FOUND=True  # set to true so we don't iterate twice

	    if NV_FOUND == False:
	        # We need to create a new normal vector
	        cur_face_normal_vec=normal_vec(
	            cur_face_normal_vec_coeff, len(self.normal_vectors) + 1)
	        self.normal_vectors.append(cur_face_normal_vec)

	    # create new face
	    new_face=face(vertex_indices, cur_face_normal_vec.index,
	                    cur_face_normal_vec.vec, relevant_v_coordinates)

	    # and append to the list of faces
	    self.faces.append(new_face)

	    return self


	def return_constant_val_along_x(self):

		min_min_v = self.vertices[self.min_min_v_i-1]

		return min_min_v.x



	def return_front_height_width(self):
		target_normal_vector=self.front_normal_vector.vec

		self.return_extreme_points()

		#save so we can restore them later
		old_min_min = copy.deepcopy(self.min_min_v)
		old_min_max = copy.deepcopy(self.min_max_v)
		old_max_max = copy.deepcopy(self.max_max_v)
		old_max_min = copy.deepcopy(self.max_min_v)

		self.rotate_front_to_default_norm()

		self.return_front_vertex_attributes()

		self.height = self.maxs[2] - self.mins[2]
		self.width  = self.maxs[1] - self.mins[1]

		self.unrotate_from_default_norm(target_normal_vector)

		self.return_extreme_points()
		final_min_min_translation = [old_min_min.x-self.min_min_v.x,
									 old_min_min.y-self.min_min_v.y,
									 old_min_min.z-self.min_min_v.z]

		self.translate_front(final_min_min_translation)

		return self

	def add_feature_to_front(self, feature, locations):
		# We need to modify vertices and faces so that we can include the relevant feature
		# need to scale and translate first!

		# locations is [x,y,z] vector X4 (4 times for luck)

		target_normal_vector=self.front_normal_vector.vec

		self.populate_output('FRONT_TEST_PRE.obj')
		
		#first, rotate front to default norm, then you have some 
		#nice properties like orthonormality....
		self.return_extreme_points()

		#save so we can restore them later
		old_min_min = copy.deepcopy(self.min_min_v)
		old_min_max = copy.deepcopy(self.min_max_v)
		old_max_max = copy.deepcopy(self.max_max_v)
		old_max_min = copy.deepcopy(self.max_min_v)
		  
		self.find_front_mp()

		self.rotate_front_to_default_norm()

		#translate so that the front min_min is exactly at origin
		min_min_origin_translation = [-self.min_min_v.x,
									  -self.min_min_v.y,
									  -self.min_min_v.z]

		self.translate_front(min_min_origin_translation)

		self.return_extreme_points()

		current_front_min_max_line = [self.min_max_v.x,
									  self.min_max_v.y,
									  self.min_max_v.z]
		

		Z_DIRECTION = self.min_max_v.z-self.min_min_v.z
		desired_front_min_max_line = [0,0,float(Z_DIRECTION)/abs(Z_DIRECTION)]
		desired_front_min_max_line = [0,0,1]

		#use numpy to create it as so! !

		AXES_TO_KEEP = [1,2] #cos X is default norm...

		orig_vector = [current_front_min_max_line[a] for a in AXES_TO_KEEP]
		target_vector = [desired_front_min_max_line[a] for a in AXES_TO_KEEP]

		cw_angle_to_move = v_angle_clockwise(orig_vector,target_vector)

		#now we also have to...take 360 minus angle_to_move cos it's counter
		#clockwise by convention...

		ccw_angle_to_move = 360-cw_angle_to_move

		#self.rotate_front_rotation_angles([ccw_angle_to_move,0,0])

		min_min_origin_translation = [-self.min_min_v.x,
									  -self.min_min_v.y,
									  -self.min_min_v.z]

		self.translate_front(min_min_origin_translation)

		self.populate_output('FRONT_TEST.obj')

		#copy the front into a dummy version to derive new min_min etc v's

		dummy_front = copy.deepcopy(self)

		dummy_front.return_extreme_points()

		#now we can be certain that it's...as required!

		#do the same for the feature...

		# we create a deep copy of it local to this function
		self.working_feature=copy.deepcopy(feature)

		# rotate it accordingly. This will update the VNs

		self.working_feature.find_joined_face_areas()
		self.working_feature.return_aspects_of_largest_faces() #return this largest one...

		self.working_feature.find_feature_mp()
		self.working_feature.center_feature()

		#now they have the same normal
		#need to make sure that front normal vector is updated, tho!

		#just use DEFAULT NORMAL VEC as planned...
		self.working_feature.rotate_feature_to_front_norm(DEFAULT_NORMAL_VEC)

		#self.working_feature.return_vertices_for_bounding_points_default_norm()

		self.working_feature.return_extreme_points()

		feature_min_min_origin_translation = [-self.working_feature.min_min_v.x,
											  -self.working_feature.min_min_v.y,
											  -self.working_feature.min_min_v.z]

		self.working_feature.translate_feature(feature_min_min_origin_translation)

		self.working_feature.populate_output('working etc.obj')
		self.working_feature.return_extreme_points()


		feature_current_front_min_max_line = [self.working_feature.min_max_v.x,
									  		  self.working_feature.min_max_v.y,
									  		  self.working_feature.min_max_v.z]

		desired_front_min_max_line = [0,0,1]

		#use numpy to create it as so! !
		AXES_TO_KEEP = [1,2] #cos X is default norm...

		feature_orig_vector = [feature_current_front_min_max_line[a] for a in AXES_TO_KEEP]
		feature_target_vector = [desired_front_min_max_line[a] for a in AXES_TO_KEEP]

		cw_angle_to_move_feature = v_angle_clockwise(feature_orig_vector,feature_target_vector)		

		ccw_angle_to_move_feature = 360 - cw_angle_to_move_feature

		#now rotate along x axis until we get to the zero point!

		self.working_feature.rotate_feature_angles([ccw_angle_to_move_feature,0,0])

		feature_min_min_origin_translation = [-self.working_feature.min_min_v.x,
											  -self.working_feature.min_min_v.y,
											  -self.working_feature.min_min_v.z]


		self.working_feature.translate_feature(feature_min_min_origin_translation)

		required_horiz_distance=locations[3][0] - locations[0][0]  # must be positive
		required_vert_distance=locations[1][1] - locations[0][1]  # must be positive

		#re-measure feature...

		self.working_feature.return_feature_lengths()

		


		scaling_horiz=required_horiz_distance / feature.horiz_length

		scaling_vert=required_vert_distance / feature.vert_length


		scaling_depth=required_vert_distance / feature.depth


		scaling_depth = scaling_vert

		# first scale
		self.working_feature.scale_feature([0.01, scaling_horiz, scaling_vert])

		#translate twice
		#first time is to align min_min
		self.return_extreme_points()
		translations1=[self.min_min_v.x -  self.working_feature.min_min_v.x,
						self.min_min_v.y  - self.working_feature.min_min_v.y,
						self.min_min_v.z  - self.working_feature.min_min_v.z]

		self.working_feature.translate(translations1)

		#translate horizontally
		vert_translation = [0,0,locations[0][1]]
		self.working_feature.translate(vert_translation)

		#translate horizontally...
		horiz_translation = [0,locations[0][0],0]
		self.working_feature.translate(horiz_translation)

		self.compare_and_update_front_working_feature_vns()

		# update vertex indices for vertices

		total_vertices_in_front = len(self.vertices)

		self.working_feature.shift_all_vertex_indices_up(total_vertices_in_front)

		# update vertex indices for faces

		self.working_feature.shift_all_feature_vertex_references_up(total_vertices_in_front)

		# append VN's to working_feature
		self.working_feature.normal_vectors=self.normal_vectors

		# Append to the list of features
		self.features.append(self.working_feature)

		# Merge it but you can keep a record of the index of the feature so you
		# can delete later
		self.features[len(self.features)-1].set_vertex_parent_index(len(self.features)+1)

		# Now we're ready to write
		self.write_feature_into_front(len(self.features) - 1)

		#rotate along default normal axis...
		self.rotate_front_rotation_angles([-ccw_angle_to_move,0,0])

		#rotate back to original orientation
		self.unrotate_from_default_norm(target_normal_vector)

		#and translate back to orig place...
		self.return_extreme_points()
		final_min_min_translation = [old_min_min.x-self.min_min_v.x,
									 old_min_min.y-self.min_min_v.y,
									 old_min_min.z-self.min_min_v.z]

		self.translate_front(final_min_min_translation)

		#and now you can return..
		self.populate_output('theee vampir2.obj')

		return self

	def write_feature_into_front(self, feature_index):

	    feature_verts=self.features[feature_index - 1].vertices

	    feature_faces=self.features[feature_index - 1].faces

	    self.vertices+=feature_verts

	    self.faces+=feature_faces

	    return self

	def compare_and_update_front_working_feature_vns(self):
	    # used for features....
	    normal_vectors_front=self.normal_vectors

	    normal_vectors_feature=self.working_feature.normal_vectors

	    feature_vn_concordance=[]

	    for feature_nv_index in range(len(normal_vectors_feature)):
	        # seeing if we can find this normal vector

	        CURRENT_FEATURE_NV_FOUND=False

	        current_feature_vec=normal_vectors_feature[feature_nv_index]
	        feature_normal_vec=current_feature_vec.vec
	        feature_normal_vec_index=current_feature_vec.index

	        for ov_index in range(len(normal_vectors_front)):
	            if normal_vectors_front[ov_index].vec == feature_normal_vec and CURRENT_FEATURE_NV_FOUND == False:
	                feature_vn_concordance.append(feature_normal_vec_index)
	                CURRENT_FEATURE_NV_FOUND=True

	        if not CURRENT_FEATURE_NV_FOUND:
	            # if it's not found in the front, we need to create it in the
	            # front....

	            # updating front VNs
	            self.normal_vectors.append(feature_normal_vec)  # put it in there!

	            # make it refer to appropriate normal vector
	            feature_vn_concordance.append(len(self.normal_vectors))

	    # now go through feature and replace the vn's accordingly....
	    self.working_feature.renumber_feature_vn_references_to_front(
	        feature_vn_concordance)

	    return self

	def delete_feature_from_front(self, feature_index):
	    # removes strings and instances of it in lists of vertices and faces and
	    # also from the features[] array!
	    vertex_indices=[]
	    for v in self.features[feature_index - 1].vertices:
	        vertex_indices.append(v.index)

	    # delert fec
	    del_face=[]
	    for f in self.faces:
	        CUR_FACE_FOUND=False
	        while CUR_FACE_FOUND == False:
	            for v in vertex_indices:
	                if v in f.face_vertex_indices:
	                    CUR_FACE_FOUND=True
	        del_face.append(CUR_FACE_FOUND)

	    face_deletion_locations=[index for index in range(len(del_face)) if del_face[index] == True]

	    # sort for paranoia....

	    ordered_face_deletion_locations=list(
	        np.argsort(deletion_locations))  # cast to list

	    # but will be same as indices themselves, so proceed...

	    rev_ordered_face_deletion_locations=ordered_deletion_locations[::-1]

	    for location in rev_ordered_face_deletion_locations:
	        del self.faces[location]

	    # delete vertices....

	    ascending_deleted_vertex_order=list(np.argsort(vertex_indices))

	    ascending_deleted_vertex_indices=[vertex_indices[order - 1] for order in ascending_deleted_vertex_order]

	    descending_deleted_vertex_indices=ascending_deleted_vertex_indices[::-1]

	    for vert_index in descending_deleted_vertex_indices:
	        del self.vertices[vert_index]

	    total_deleted_vertices=len(vertex_indices)

	    start_vertex_decrement=descending_deleted_vertex_indices[0] + 1

	    # modify vertices in front
	    for v in self.vertices:
	        if v.index >= start_vertex_decrement:
	            # cut down here...

	            v.shift_index_down(total_deleted_vertices)
	            v.parent_index -= 1  # decrement the parent index cos we deleted one!

	    # modify faces in front
	    for face_index in range(ordered_face_deletion_locations[0], len(self.faces)):
	        f.shift_all_face_vertex_references_down(total_deleted_vertices)


	    # modify vertices in remaining features

	    for extra_f_i in range(feature_index, len(self.features) - 1):
	        current_feature=self.features[extra_f_i]

	        for v in current_feature.vertices:
	            v.shift_index_down(total_deleted_vertices)
	            v.parent_index -= 1  # decrement the parent index cos we deleted one!

	        for f in current_feature.faces:
	            f.shift_all_face_vertex_references_down(total_deleted_vertices)

	    # modify faces in remaining features

	    # finally, delete the feature from list!

	    del self.features[feature_index - 1]

	    return self


	def populate_output(self,OUT_FN):
		out_lines = open(OUT_FN,'w+')
		#now write vn's...
		for vn in self.normal_vectors:
			out_lines.write(vn.vec_str + '\n')
		#now write vertices...
		for vert in self.vertices:
			out_lines.write(vert.vertex_str + '\n')
		#now write faces...
		for face in self.faces:
			out_lines.write(face.face_str+ '\n')

		out_lines.close()