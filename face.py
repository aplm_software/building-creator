from reqs import *

class face:
	def __init__(self, vertex_indices, normal_vec_index, normal_vector_vec, vertex_coordinates):
		
		self.face_vertex_indices=vertex_indices #list of ints starting from 1

		self.normal_vec_index=normal_vec_index #starting from 1, can't be a list

		self.normal_vector_vec = normal_vector_vec #is in the format [x,y,z]

		self.vertex_coordinates = vertex_coordinates #a list in the format[[x1,y1,z1],[x2,y2,z2],...]

		self.form_face_str() #form the face string

		self.return_min_max() #returns extreme points
		self.measure_face_surface_area() #create surface area...

	def shift_all_face_vertex_references_up(self,shift_amt):
		self.face_vertex_indices = [cv + shift_amt for cv in self.face_vertex_indices]
		self.form_face_str() #now reset the face string! brill!
		return self

	def shift_all_face_vertex_references_down(self,shift_amt):
		self.face_vertex_indices = [cv - shift_amt for cv in self.face_vertex_indices]
		self.form_face_str() #now reset the face string! brill!
		return self

	def decrement_vertices_deletion(self,del_v):
	#to be used to reassign refs to vertices in faces when they are delet from obj!
		cur_vs = [int(fv) for fv in self.face_vertex_indices]

		out_vs = []
		for cv in cur_vs:
			out_v = cv
			if cv > del_v:
				out_v-=1
				out_vs.append(out_v)

				self.face_vertex_indices = [str(nv) for nv in out_vs]

		self.form_face_str() #now reset face string! brill!

		return self

	def replace_vertex(self,orig_vertex,rep_vertex):
		if str(orig_vertex) in self.face_vertex_indices:
			self.face_vertex_indices = [rep_vertex if v_i == orig_vertex  else v_i for v_i in self.face_vertex_indices]
		self.form_face_str() #reset face string
		return self

	def form_face_str(self):
		#forms the face string for writing out..
		self.face_str = 'f '
		for v_i in self.face_vertex_indices:
			self.face_str = self.face_str + str(v_i) + '//' + str(self.normal_vec_index) + ' '
		self.face_str = self.face_str[:-1] #delete last space
		return self

	def return_min_max(self):

		xs = [v[0] for v in self.vertex_coordinates]
		ys = [v[0] for v in self.vertex_coordinates]
		zs = [v[0] for v in self.vertex_coordinates]

		self.min_x = min(xs)
		self.max_x = max(xs)
		self.min_y = min(ys)
		self.max_y = max(ys)
		self.min_z = min(zs)
		self.max_z = max(zs)

		return self

	def measure_face_surface_area(self):
		#first we create a copy and rotate....
		mps = return_mp_xyz(self.vertex_coordinates)


		moving_points = [-mp for mp in mps]
		dummy_vertices = []

		for v in self.vertex_coordinates:
			dummy_vertices.append(vertex(v[0],v[1],v[2],len(dummy_vertices)+1,'DUMMY',-1))

		for dummy_v in dummy_vertices:
			dummy_v.change_all_axes_relative(moving_points)

		#return the angle we need to rotate to
		rotation_matrix = inverse_rotation_matrix(self.normal_vector_vec,DEFAULT_NORMAL_VEC)

		#for dummy_v in dummy_vertices:
		#	print(dummy_v.get_vertex_vector())


		#now rotate....
		for dummy_v in dummy_vertices:
			dummy_v.rotate_3d_matrix(rotation_matrix)
		#now our normal axis' index is 0


		new_dummy_v_verts = [dummy_v.get_vertex_vector() for dummy_v in dummy_vertices]

		#do a check....
		dummy_x_val = [dummy_v.x for dummy_v in dummy_vertices]
		if (max(dummy_x_val)-min(dummy_x_val))>EPS_ROTATION:
			print('warning: x axis not uniform for rotated dummy vertices when defining face surface area')
			print('here are dummy x val')
			print(dummy_x_val)
			print('max x val is: {:.4f}'.format(max(dummy_x_val)))
			print('min x val is: {:.4f}'.format(min(dummy_x_val)))
			print('for EPS_ROTATION of: {:.4f}'.format(EPS_ROTATION))

		horiz_vals = [vc[1] for vc in new_dummy_v_verts]
		vert_vals =  [vc[2] for vc in new_dummy_v_verts]

		#will need to be ordered to define the face in the first place, so can therefore just
		#pipe it into this function.

		#print('here indiv horiz and verr')
		#print(horiz_vals)
		#print(vert_vals)
		self.surf_area = PolyArea(horiz_vals,vert_vals)

		#print('surf area is: {:.4f}'.format(self.surf_area))

		return self

	def update_vertex_coordinates(self,new_coordinates):
		self.vertex_coordinates = new_coordinates

		return self

	def return_vertex_coordinates(self):

		return self.vertex_coordinates

	def doesFaceContainVertex(self,in_vertex_vec):
		#derive an equation representing the plane in 3dimensions
		self.face_plane_eq, self.face_plane_eq_val = return_plane_equation(self.vertex_coordinates)

		#now test it!
		current_vertex_val = self.face_plane_eq(in_vertex_vec)

		if abs(self.face_plane_eq_val - current_vertex_val) < EPS_PLANE_CONTAIN:
			#check individual axes to ensure that the point lies within the limits
			if ((self.min_x <= in_vertex_vec[0] <= self.max_x) and 
				(self.min_y <= in_vertex_vec[1] <= self.max_y) and 
				(self.min_z <= in_vertex_vec[2] <= self.max_z)):
				return True
		else:
			return False

	def return_vert_indices(self):
		#returns the indices of face vertices, starting from 1...
		return self.face_vertex_indices

	def update_vn_ref(self,target_bijections):
		#print('old face str')
		#print(self.face_str)
		for pos_val in range(len(target_bijections)):
			pos_val = int(pos_val)+1
			if self.normal_vec_index==pos_val:
				new_nvs = target_bijections[pos_val-1]
				self.normal_vec_index = new_nvs
				self.form_face_str()
				#print('new face str')
				#print(self.face_str)
				return self #only needs to iterate through once
