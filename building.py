from reqs import *


class building:
	def __init__(self):

		self.fronts=[]

		self.normal_vectors=[]

		self.features=[]

		#normal vectors reconciled at the end of the process...


	def unify_fronts(self):

		self.normal_vectors_for_print = []
		self.faces_for_print = []
		self.vertices_for_print = []

		#1. just add every face into the original front
		unified_building = copy.deepcopy(self.fronts[0])

		for front_i in range(len(self.fronts)):
			if front_i>0:
				unified_building.add_front(self.fronts[front_i])

		self.normal_vectors_for_print = unified_building.normal_vectors

		self.faces_for_print = unified_building.faces

		self.vertices_for_print = unified_building.vertices


	def populate_output(self,OUT_FN):
		self.unify_fronts() #need to do this
		#open the file firste
		out_lines = open(OUT_FN,'w+')
		#now write vn's...
		for vn in self.normal_vectors_for_print:
			out_lines.write(vn.vec_str + '\n')
		#now write vertices...
		for vert in self.vertices_for_print:
			out_lines.write(vert.vertex_str + '\n')
		#now write faces...
		for face in self.faces_for_print:
			out_lines.write(face.face_str+ '\n')

		out_lines.close()

	#adds the feature to the list of features that's in the building
	def add_feature_to_building(self,FEATURE_FN):
		#for loading in a feature from an external file!
		self.features.append(feature(FEATURE_FN)) #add feature to list of features...

		return self

	def compare_normal_vectors_front(self,front_index):
		#used for features....
		original_normal_vectors = self.normal_vectors
		front_normal_vectors = self.fronts[front_index].normal_vectors
		out_concordance=[]

		for f_nv_i in range(len(front_normal_vectors)):
			f_vec = v_feat[f_nv_i].vec
			f_index = v_feat[f_nv_i].index
			CURRENT_NV_FOUND=False
			for b_nv_i in range(len(original_normal_vectors)):
				if original_normal_vectors[b_nv_i].vec==f_vec and CURRENT_NV_FOUND==False:
					out_concordance.append(b_nv_i+1)
					CURRENT_NV_FOUND=True

			if CURRENT_NV_FOUND==False:
				#we have to add it in...

				self.normal_vectors.append(front_normal_vectors[f_nv_i])

				out_concordance.append(len(original_normal_vectors))

		self.fronts.front_building_vn_concordance=out_concordance

		return self


	def reset_normal_vectors_front(self,front_index):

		self.fronts.reset_normal_vectors_to_building()

		self.fronts.normal_vectors = self.normal_vectors

		return self

	def reset_all_front_normal_vectors(self):

		for f_i in range(len(self.fronts)):
			self.compare_normal_vectors_front(f_i)
		for f_i in range(len(self.fronts)):
			self.reset_normal_vectors_front(f_i)

		return self


	def add_blank_front(self,dims):
		self.fronts.append(front(dims))

		return self

	def modify_NVIs(self):

		#we modify NVIs accordingly. What does NVIs stand for?
		for k in range(8):
			self.nvis[k][0] = self.v_ind+k #we apppend the index to it....
			self.nvis[k][self.rel_ax_ind+1]=self.const_ax_val #we make necessary index the const ax val......

		#need to increement by 1 cos the first array element is the vertex index!
		r_ind = [r + 1 for r in self.remaining_axes] 

		self.nvis[0][r_ind[0]]=self.current_cut.min_max[0] 		#horiz
		self.nvis[0][r_ind[1]]=self.current_cut.min_max[1]		#vert 

		self.nvis[1][r_ind[0]]=self.current_cut.min_max[0] 		#horiz
		self.nvis[1][r_ind[1]]=self.current_front.min_max[1]	#vert 

		self.nvis[2][r_ind[0]]=self.current_cut.max_max[0] 		#horiz
		self.nvis[2][r_ind[1]]=self.current_front.max_max[1]	#vert 

		self.nvis[3][r_ind[0]]=self.current_cut.max_max[0] 		#horiz
		self.nvis[3][r_ind[1]]=self.current_cut.max_max[1]		#vert 

		self.nvis[4][r_ind[0]]=self.current_cut.min_min[0] 		#horiz
		self.nvis[4][r_ind[1]]=self.current_front.min_min[1]	#vert 

		self.nvis[5][r_ind[0]]=self.current_cut.min_min[0] 		#horiz
		self.nvis[5][r_ind[1]]=self.current_cut.min_min[1]		#vert 

		self.nvis[6][r_ind[0]]=self.current_cut.max_min[0] 		#horiz
		self.nvis[6][r_ind[1]]=self.current_cut.max_min[1]		#vert 

		self.nvis[7][r_ind[0]]=self.current_cut.max_min[0] 		#horiz
		self.nvis[7][r_ind[1]]=self.current_front.max_min[1]	#vert 

		return self

	def create_new_NVI_vertices(self):
		for nv in range(8):
			#create new vertex.....
			self.vertices.append(vertex(self.nvis[nv][1],
										self.nvis[nv][2],
										self.nvis[nv][3],
										self.nvis[nv][0]))
		return self

	def create_four_NVI_faces(self):
		#FACES add them!!
		#Fairly arbitrary allocation of face indices, ugly - revise
		self.faces.append(face([self.cf_min_min_i,self.cf_min_max_i,self.nvis[1][0],self.nvis[4][0]],self.face_i.normal_vec,self.vertices)) #face1
		self.faces.append(face([self.nvis[0][0],self.nvis[1][0],self.nvis[2][0],self.nvis[3][0]],self.face_i.normal_vec,self.vertices)) 	#face2
		self.faces.append(face([self.nvis[7][0],self.nvis[2][0],self.cf_max_max_i,self.cf_max_min_i],self.face_i.normal_vec,self.vertices))	#face3
		self.faces.append(face([self.nvis[4][0],self.nvis[5][0],self.nvis[6][0],self.nvis[7][0]],self.face_i.normal_vec,self.vertices)) 	#face4
		return self
				
	def cut_hole(self, horiz_lower, vert_lower, horiz_upper, vert_upper, front_num):

		relevant_faces=copy.deepcopy(self.fronts[front_num-1]) #so will contain all the relevant fronts....

		self.rel_ax_ind = self.faces[int(relevant_faces[0])-1].return_rel_ax_ind(self.vertices) #can take any face contained within the front...

		first_v_pts = copy.deepcopy(self.vertices[int(self.faces[int(relevant_faces[0])-1].face_verts[0])-1].vert)

		self.const_ax_val = copy.deepcopy(first_v_pts[self.rel_ax_ind])

		v1_vec=[0,0,0]
		v2_vec=[0,0,0]
		v3_vec=[0,0,0]
		v4_vec=[0,0,0]

		self.remaining_axes=[0,1,2]

		self.remaining_axes.remove(self.rel_ax_ind) #remove this fockere! move it!

		#just assume that min and max are the same...
		v1_vec[self.rel_ax_ind]=self.const_ax_val
		v2_vec[self.rel_ax_ind]=self.const_ax_val
		v3_vec[self.rel_ax_ind]=self.const_ax_val
		v4_vec[self.rel_ax_ind]=self.const_ax_val

		v1_vec[self.remaining_axes[0]]=horiz_lower
		v1_vec[self.remaining_axes[1]]=vert_lower

		v2_vec[self.remaining_axes[0]]=horiz_lower
		v2_vec[self.remaining_axes[1]]=vert_upper

		v3_vec[self.remaining_axes[0]]=horiz_upper
		v3_vec[self.remaining_axes[1]]=vert_upper

		v4_vec[self.remaining_axes[0]]=horiz_upper 
		v4_vec[self.remaining_axes[1]]=vert_lower


		v1 = vertex(v1_vec[0],v1_vec[1],v1_vec[2],1)
		v2 = vertex(v2_vec[0],v2_vec[1],v2_vec[2],1)
		v3 = vertex(v3_vec[0],v3_vec[1],v3_vec[2],1)
		v4 = vertex(v4_vec[0],v4_vec[1],v4_vec[2],1)

		self.current_cut = cut_hole(v1,v2,v3,v4,self.faces[int(relevant_faces[0])-1].normal_vec,self.rel_ax_ind)


		for front_cut in self.front_cuts[front_num-1]:
			if self.current_cut.intersect(front_cut) == True:
				print('error the hole intersects one that has already been cut')
				return False

		#now we need to find out affected faces and it should theoretically only be ONE face...
		face_selected=False
		for f_i in relevant_faces: #relevant_faces is a list of face indices...starting from this 1 index!
			#f_i = int
			#print('doing face: ' + str(f_i))

			face_i = self.faces[f_i-1]

			MM_VEC = [False] * 4

			#reset device!
			self.min_min_face_i=None
			self.min_max_face_i=None
			self.max_max_face_i=None
			self.max_min_face_i=None

			#need to not consider faces already deleted!!! so check self.deletion_faces
			if face_i.vertex_inside_face(self.vertices,self.rel_ax_ind,self.current_cut.v1,False)=="inside" and f_i not in self.deletion_faces:
				MM_VEC[0]=True
				self.min_min_face_i=f_i #face index...

			if face_i.vertex_inside_face(self.vertices,self.rel_ax_ind,self.current_cut.v2,False)=="inside" and f_i not in self.deletion_faces:
				MM_VEC[1]=True
				self.min_max_face_i=f_i #face index...

			if face_i.vertex_inside_face(self.vertices,self.rel_ax_ind,self.current_cut.v3,False)=="inside" and f_i not in self.deletion_faces:
				MM_VEC[2]=True
				self.max_max_face_i=f_i #face index..
				
			if face_i.vertex_inside_face(self.vertices,self.rel_ax_ind,self.current_cut.v4,False)=="inside" and f_i not in self.deletion_faces:
				MM_VEC[3]=True
				self.max_min_face_i=f_i #face index...

			if len(list(set(MM_VEC)))==1 and MM_VEC[0]==True:

				all_mm_fs = [self.min_min_face_i,
							  self.min_max_face_i,
							  self.max_max_face_i,
							  self.max_min_face_i]

				if all_mm_fs[0]==all_mm_fs[1]==all_mm_fs[2]==all_mm_fs[3]:

					if face_selected==False:
						relevant_face_i = all_mm_fs[0]
						face_selected=True


			else:
				sdjfklj=1


			if face_selected==True:
				break
		if face_selected==False:
			print('\n\n\t\tCRITICAL ERROR NO FACE FOUND!!\n\n')

		#rel vertices....
		relevant_vertices = []
		relevant_face_i = [relevant_face_i] #just for quick hacke
		for f_i in relevant_face_i:
			face_i = self.faces[f_i-1]
			relevant_vertices = relevant_vertices + face_i.face_verts

		self.face_i = face_i #quick hackere

		relevant_face_i = relevant_face_i[0]

		for v_index in relevant_vertices:
			v = self.vertices[v_index-1]			#cos v_index starts from 1
			if v_index == relevant_vertices[0]:
				min_x = v.x
				max_x = v.x
				min_y = v.y
				max_y = v.y
				min_z = v.z
				max_z = v.z
			else:
				if v.x>max_x:
					max_x=v.x
				if v.x<min_x:
					min_x=v.x
				if v.y>max_y:
					max_y=v.y
				if v.y<min_y:
					min_y=v.y
				if v.z>max_z:
					max_z=v.z
				if v.z<min_z:
					min_z=v.z


		rel_face_norm = self.norm_vectors[face_i.normal_vec-1] #normal_vec is the indexe but must subtracte 1!

		v1_vec=[0,0,0]
		v2_vec=[0,0,0]
		v3_vec=[0,0,0]
		v4_vec=[0,0,0]

		self.remaining_axes=[0,1,2]

		mins = [min_x,min_y,min_z]	#mins
		maxs = [max_x,max_y,max_z]	#maxes

		self.remaining_axes.remove(self.rel_ax_ind) #remove this fockere! move it!

		self.const_ax_val = [min_x,min_y,min_z][self.rel_ax_ind]
		#just assume that min and max are the same...
		v1_vec[self.rel_ax_ind]=self.const_ax_val
		v2_vec[self.rel_ax_ind]=self.const_ax_val
		v3_vec[self.rel_ax_ind]=self.const_ax_val
		v4_vec[self.rel_ax_ind]=self.const_ax_val

		v1_vec[self.remaining_axes[0]]=horiz_lower
		v1_vec[self.remaining_axes[1]]=vert_lower

		v2_vec[self.remaining_axes[0]]=horiz_lower
		v2_vec[self.remaining_axes[1]]=vert_upper

		v3_vec[self.remaining_axes[0]]=horiz_upper
		v3_vec[self.remaining_axes[1]]=vert_upper

		v4_vec[self.remaining_axes[0]]=horiz_upper 
		v4_vec[self.remaining_axes[1]]=vert_lower


		v1 = vertex(v1_vec[0],v1_vec[1],v1_vec[2],1)
		v2 = vertex(v2_vec[0],v2_vec[1],v2_vec[2],1)
		v3 = vertex(v3_vec[0],v3_vec[1],v3_vec[2],1)
		v4 = vertex(v4_vec[0],v4_vec[1],v4_vec[2],1)

		self.current_cut = cut_hole(v1,v2,v3,v4,face_i.normal_vec,self.rel_ax_ind)

		for front_cut in self.front_cuts[front_num-1]:
			if self.current_cut.intersect(front_cut) == True:
				print('error the hole intersects one that has already been cut')
				return False

		if self.rel_ax_ind==0: #x is norme
			min_horiz = min_y
			max_horiz = max_y
			const_ax_val = min_x
			min_vert = min_z
			max_vert = max_z

			for v_index in relevant_vertices: #face_verts starts from 1...
				v = self.vertices[v_index-1]
				if v.y==min_y and v.z==min_z:
					self.cf_min_min_i=v.index
				if v.y==min_y and v.z==max_z:
					self.cf_min_max_i=v.index
				if v.y==max_y and v.z==min_z:
					self.cf_max_min_i=v.index
				if v.y==max_y and v.z==max_z:
					self.cf_max_max_i=v.index

		if self.rel_ax_ind==1: #y is norme
			min_horiz = min_x
			max_horiz = max_x
			const_ax_val = min_y
			min_vert = min_z
			max_vert = max_z
			for v_index in relevant_vertices:
				v = self.vertices[v_index-1]
				if v.x==min_x and v.z==min_z:
					self.cf_min_min_i=v.index
				if v.x==min_x and v.z==max_z:
					self.cf_min_max_i=v.index
				if v.x==max_x and v.z==min_z:
					self.cf_max_min_i=v.index
				if v.x==max_x and v.z==max_z:
					self.cf_max_max_i=v.index
			
		if self.rel_ax_ind==2: 
			print('fucked it!!!! go back!')

		min_min = self.vertices[int(self.cf_min_min_i)-1].return_normed_axes(self.rel_ax_ind) #2d vector
		min_max = self.vertices[int(self.cf_min_max_i)-1].return_normed_axes(self.rel_ax_ind) #2d vector
		max_max = self.vertices[int(self.cf_max_max_i)-1].return_normed_axes(self.rel_ax_ind) #2d vector
		max_min = self.vertices[int(self.cf_max_min_i)-1].return_normed_axes(self.rel_ax_ind) #2d vector

		#set the current front
		self.current_front = front(min_min,max_min,max_max,min_max,face_i.normal_vec,self.rel_ax_ind) #this is global and goes over all faces in the current front....
		#count num of vertices! important!
		self.get_max_vertex_index()
		#create 8 new nvis for this fockere
		self.create_new_nvis()
		#modify NVIs so that they contain appropriate indices
		self.modify_NVIs()
		#so now need to create new vertices based on the NVIs we just instantiated and set vals for...
		self.create_new_NVI_vertices()
		#now we add new faces based on these NVI vertices....
		self.create_four_NVI_faces()
		#reset the fronts list to contain these faces
		#store old fronts
		old_fronts = copy.deepcopy(self.fronts[front_num-1]) #create deeepe thee dep
		#remove
		old_fronts.remove(relevant_face_i)
		self.fronts[front_num-1] = old_fronts + [k for k in range(len(self.faces)-3,len(self.faces)+1)] #this is the new faces list..... the 4 most-recently added vertices
		self.tot_faces[front_num-1]=list(set(self.tot_faces[front_num-1]).union(set(self.fronts[front_num-1]))) #update total num faces!!
		#append current_cut to the list of cuts so that we can check for intersection in future
		self.front_cuts[front_num-1].append(self.current_cut) 
		#append deletion face index to list so that we know to delete them at the end
		self.deletion_faces.append(relevant_face_i)


		#create all_faces_list......

		all_faces = list(chain(*self.fronts))

		print_it=0
		if print_it==1:

			print('')
			print('\tHOLE SUCCESSFULLY CUT!')
			print('------------------------------------')
			print('horiz_lower: \t\t\t' + str(horiz_lower))
			print('horiz_upper: \t\t\t' + str(horiz_upper))
			print('vert_lower: \t\t\t' +  str(vert_lower))
			print('vert_upper: \t\t\t' +  str(vert_upper))
			print('front num: \t\t\t' +   str(front_num))

		account_total = self.deletion_faces + self.fronts[front_num-1]
		account_total.sort()

		total_cuts = [len(tf) for tf in self.front_cuts]

		return self

	def delete_extra_faces(self):
		self.deletion_faces.sort(reverse=True)
		df = [d - 1 for d in self.deletion_faces]
		for f_index in df:
			del self.faces[f_index]
			#now reset front liste!
			out_front = []
			for front in self.fronts:
				out_fs = []
				for face in front:
					if face> f_index+1:
						face = face - 1
					out_fs.append(face)

				out_front.append(out_fs)

			self.fronts=out_front

		self.deletion_faces  = [] #empty the list incase method called again
		return self


	def return_building_dim(self):
		return self.xlim, self.ylim, self.zlim


	def center_front(self,front_index):
		return self.center_front_faces(self.fronts[front_index-1])

	def center_front_faces(self, f_is):
		#return all vertex indices associated with these faces
		mps = self.find_fs_mp(f_is)

		change_amt = [-m for m in mps]

		relevant_vertices = []
		for f in f_is:
			face_index = int(f)-1 #cos index needs to start at 0 but in the front list it start = 1

			f_verts = []
			for v in self.faces[face_index].face_verts:
				v_index = int(v) - 1	
				relevant_vertices.append(self.vertices[v_index])

		relevant_vertices = list(set(relevant_vertices)) #remove duplicates!!!

		for v in relevant_vertices:
			v.change_all_axes_rel(change_amt)

		return self

	def rotate_front_faces(self, f_is, rotation_angles):
		#return all vertex indices associated with these faces
		mps = find_fs_mp(f_is)
		self.find_feature_mp()
		change_amt = [-m for m in mps]
		relevant_vertices = []
		for f in f_is:
			face_index = int(f)-1 #cos index needs to start at 0 but in the front list it start = 1
			f_verts = []
			for v in self.faces[face_index].face_verts:
				v_index = int(v) - 1	
				relevant_vertices.append(self.vertices[v_index])

		relevant_vertices = list(set(relevant_vertices)) #remove duplicates!!!

		for v in relevant_vertices:
			v.rotate(rotation_angles)

		return self

	def return_front_min_min(self,FRONT_NUM):
		#to be used when we are stitching fronts togeth!
		front_v_i = self.return_front_v_i(FRONT_NUM)

		min_v_i = min(front_v_i)

		min_x = self.vertices[min_v_i-1].x
		min_y = self.vertices[min_v_i-1].y

		return [min_x,min_y]

	def return_front_vertices(self,FRONT_NUM):
		front_v_i = self.return_front_v_i(FRONT_NUM)

		front_v_i.sort() #maybe sorting is necessary?

		front_vertices = [self.vertices[v-1] for v in front_v_i]

		return front_vertices

	def stitch_top_bottom(self):
		#find  all min min and make a new face....

		#just call NV 5 for now...
		NV=5
		min_min_i=[]
		min_max_i=[]
		#create two new faces..
		for fr_i in range(len(self.fronts)):
			if fr_i==0:
				print('not this one')
			else:
				f_verts = self.return_front_v_i(fr_i+1)
				f_verts.sort()

				min_min_i.append(int(f_verts[0]))
				min_max_i.append(int(f_verts[3]))
			
		
		print('mins maxes')
		print(min_min_i)
		print(min_max_i)

		self.add_face(min_min_i,5)
		self.add_face(min_max_i,5)
		return self

	def translate_front(self, FRONT_NUM, change_amt):

		self.fronts[f_i-1].translate_front(change_amt)

		return self


	def recast_normal_vectors(self,rotation):
		# but new face will have to have its references to them updated

		new_vn_vecs = []
		for vn in self.norm_vectors:
			new_vn_vecs.append(vn.rotate(rotation))

		vn_map_additions=[]
		for new_vn_vec in new_vn_vecs:
			current_addition=None #null in this instance
			for feature_vn in self.norm_vectors:
				if feature_vn.is_same(new_vn_vec):
					current_addition=feature_vn.index

			if current_addition==None:
				#find index of new vector
				new_vn_index = len(self.norm_vectors)+1
				print('it does not exist so we made a new one!')

				#create new vector
				self.norm_vectors.append(normal_vec(new_vn_vec, new_vn_index))
				
				#remap this vn badboy honga!
				vn_map_additions.append(new_vn_index)

			else:
				vn_map_additions.append(current_addition+1)

		vn_map_additions = [vn for vn in vn_map_additions]

		vn_mp = list(np.argsort(vn_map_additions))

		vn_mp = [vn + 1 for vn in vn_mp] #need to srt them...

		self.vn_map_additions = vn_mp # for use when we remap the faces
		print('mappings are here:')
		print(self.vn_map_additions)
		return self

	def renumber_front_vn_references(self,front_num):

		#need to make sure we select face indices from this front ONLY
		relevant_face_i  = [f-1 for f in self.fronts[front_num-1]] #being careful to index from 0

		for f_i in range(len(self.faces)):
			if f_i in relevant_face_i:
				f = self.faces[f_i] #facessss refo!
				f.update_vn_ref(self.vn_map_additions,self.vertices)

		self.vn_map_additions=[] #clear this fucking thing out! So we don't iterate twice!

		return self

	def find_front_mp(self,f_i):
		self.fronts[f_i-1].find_front_mp()
		return self

	def center_front(self, FRONT_NUM):
		self.fronts[f_i].center_front()
		return self

	def rotate_front_angles(self,FRONT_NUM, rotations):
		#return all faces indices associated w this particular front!
		front_v_i = self.return_front_v_i(FRONT_NUM)
		for v_i in front_v_i:
			self.vertices[v_i-1].rotate(rotations)
		return self


	def stitch_fronts_default_pattern(self,FRONT_NUM,num_fronts):

		front_norm_ind = self.return_front_norm_ind(FRONT_NUM)

		intervalse = self.return_front_ranges(FRONT_NUM)

		new_intervals = [intervalse[k] for k in range(len(intervalse)) if k!= front_norm_ind]

		front_len = new_intervals[0]
		print('front len is')
		print(front_len)

		rotation_angles = return_rotation_angles(num_fronts) #and rotating around z so...

		front_mins = self.return_front_mins(FRONT_NUM)
		front_maxs = self.return_front_maxs(FRONT_NUM)
		start_x = front_maxs[0]
		start_y = front_mins[1]
		print('start x and y')
		print(start_x)
		print(start_y)
		#polygon points return them!
		polygon_points = return_polygon(num_fronts,start_x,start_y,front_len)

		min_min_pts = self.return_front_min_min(FRONT_NUM)


		for k in range(num_fronts):
			self.duplicate_front(FRONT_NUM)
			#rotate front to angle
			#translate the front as req!

			rotation_vec = [0,0,rotation_angles[k]]

			self.rotate_front(k+2,[0,0,rotation_angles[k]])

			print('len of norm v')
			print(len(self.norm_vectors))

			trans_x = polygon_points[k][0]#-min_min_pts[0]
			trans_y = polygon_points[k][1]#-min_min_pts[1]
			trans_z = 0

			trans_vector = [trans_x[0], trans_y[0], trans_z]
			#and then translate
			#self.translate_front(k+2,trans_vector)

			#try stitching a different way....


			next_min_min=self.return_front_vertices(len(self.fronts))[0]

			prev_max_min=self.return_front_vertices(len(self.fronts)-1)[1]



			trans_vector = [float(prev_max_min.x) - float(next_min_min.x),
							float(prev_max_min.y) - float(next_min_min.y),
							float(prev_max_min.z) - float(next_min_min.z)]

			self.translate_front(k+2,trans_vector)

			print('trans vector is')
			print(trans_vector)

			print('after translatoin')
			print('xs')
			print(prev_max_min.x) #why does prev max_min change?
			print(next_min_min.x)

			print('ys')
			print(prev_max_min.y)
			print(next_min_min.y)


		return self


	def rotate_front_rotation_angles(self,FRONT_NUM, rotation_angles):

		self.fronts[FRONT_NUM-1].rotate_front_rotation_angles(rotation_angles)
		return self

	def remove_front_completely(self,f_i):

		del self.fronts[f_i-1]
