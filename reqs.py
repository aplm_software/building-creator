import math
import numpy as np
import copy 
import collections
import re
import copy
from 	itertools 		import chain, compress
import random
import os

ov = np.array([-1,0,0])
iv = np.array([1,0,0])

from bitstring import BitStream, BitArray #maybe not needed anymore

#Constants
DEFAULT_NORMAL_VECTOR_INDEX = 2 	 #default vn has this one loaded in second
DEFAULT_NORMAL_VEC = [1,0,0] #using x axis as default norm
EPS_ROTATION = 0.1 #for checking of dummy rotated vertices in calc of surface area for faces
EPS_PLANE_CONTAIN = 0.001 #for checking that a point lies within a plane




#Functions


def distance(p1, p2):
	return math.hypot(float(p1[0])-float(p2[0]), float(p1[1])-float(p2[1]))

#extra functions!
def triang_area(a, b, c):
    side_a = distance(a, b)
    side_b = distance(b, c)
    side_c = distance(c, a)
    s = 0.5 * ( side_a + side_b + side_c)
    return math.math.sqrt(s * (s - side_a) * (s - side_b) * (s - side_c))


#functions for operations on vectors in 2d
def v_length(v):
	return math.sqrt(v[0]**2+v[1]**2)

def v_dot_product(v,w):
	return v[0]*w[0]+v[1]*w[1]

def v_determinant(v,w):
	return v[0]*w[1]-v[1]*w[0]

def v_inner_angle(v,w):
	cosx=v_dot_product(v,w)/(v_length(v)*v_length(w))
	rad=math.acos(cosx) # in radians
	return rad*180/math.pi # returns degrees

def v_angle_clockwise(A, B):
	same_vec=[]
	for k_ind in range(len(A)):
		if A[k_ind]==B[k_ind]:
			same_vec.append(1)
		else:
			same_vec.append(0)
	if list(set(same_vec))==[1]:
		return 0
	else:
		inner=v_inner_angle(A,B)
		det = v_determinant(A,B)
		if det<0: #this is a property of the det. If the det < 0 then B is clockwise of A
			return inner
		else: # if the det > 0 then A is immediately clockwise of B
			return 360-inner

def vector_length_3d(in_vec):
	return math.sqrt(in_vec[0]**2 + in_vec[1]**2 + in_vec[2]**2)

#calculate polygon area!
def PolyArea(x,y):
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))

def reg_split(in_str):
	#Splits a string into constituent parts as req. for parsing
	in2_str=re.sub(' +',' ',in_str)
	out =re.sub('\t',' ',in2_str).split(' ')
	return out

def srange(k):
	out_lis = [s for s in range(k)]
	out_lis=out_lis[1:] + [k]
	return out_lis
	
def return_mp(in_list):
	in_list = [float(x) for x in in_list]

	max_l = max(in_list)
	min_l = min(in_list)

	if max_l==min_l:
		avg = 0 #just return it as the same cos no difference!
	else:
		avg = (max_l - min_l)/2 #half way between!
	return min_l + avg

def return_mp_xyz(vertex_coordinates):

	xs = [v[0] for v in vertex_coordinates]
	ys = [v[1] for v in vertex_coordinates]
	zs = [v[2] for v in vertex_coordinates]

	return [return_mp(xs),
			return_mp(ys),
			return_mp(zs)]


def str_format(in_num):
	#used for priting decimals to file
	out_str = ''
	if isinstance(in_num,list):
	 	out_str = '['
	 	for n in in_num:
	 		out_str = out_str + " %.2f," % float(n)
	 	#remove last comma from the string....
	 	out_str = out_str[:-1] + ' ]'
	else:
		out_str = "%.2f" % float(in_num)
	return out_str

#vector rotation functions
def cos_d(angle):
	#angle is in degrees
	#you need to convert degrees to radians so that you can input to math.cos function!
	return math.cos(math.radians(angle))

def sin_d(angle):
	#angle is in degrees
	#you need to convert degrees to radians so that you can input to math.sin function!
	return math.sin(math.radians(angle))

def x_rotation_matrix(angle):
	col_1=[1,            0,             0]
	col_2=[0, cos_d(angle), -sin_d(angle)]
	col_3=[0, sin_d(angle),  cos_d(angle)]

	out_mat = np.array([col_1, col_2, col_3])
	return out_mat

def y_rotation_matrix(angle):

	col_1=[cos_d(angle),  0, sin_d(angle)]
	col_2=[0, 			  1, 	   		0]
	col_3=[-sin_d(angle), 0, cos_d(angle)]

	out_mat = np.array([col_1, col_2, col_3])
	return out_mat

def z_rotation_matrix(angle):

	col_1=[cos_d(angle), -sin_d(angle), 0]
	col_2=[sin_d(angle),  cos_d(angle), 0]
	col_3=[0, 		   0,  			    1]

	out_mat = np.array([col_1, col_2, col_3])
	return out_mat

def rotate_all_axes(input_vec, angles):
	rot    = input_vec
	xrot   = input_vec.dot(x_rotation_matrix(angles[0]))
	xyrot  = xrot.dot(y_rotation_matrix(angles[1]))
	xyzrot = xyrot.dot(z_rotation_matrix(angles[2]))
	return xyzrot

def return_rotation_matrix_3d(angles):
	#for a list of angles in the form [x,y,z], returns the rotation matrix
	x_rot_m = x_rotation_matrix(float(angles[0]))
	y_rot_m = y_rotation_matrix(float(angles[1]))
	z_rot_m = z_rotation_matrix(float(angles[2]))
	xy_rot_m =np.matmul(x_rot_m,y_rot_m)
	xyz_rot_m = np.matmul(z_rot_m,xy_rot_m)
	return xyz_rot_m

def return_rotation_matrix_2d(angle):
	col_1 =[cos_d(angle), sin_d(angle)]
	col_2 = [sin_d(angle), cos_d(angle)]
	out_mat = np.array([col_1,col_2])
	return out_mat

#A Procedure to draw any regular polygon with 3 or more sides.
def return_polygon(numberOfSides,start_x,start_y,front_len):
	positions=[]

	current_position = np.array([float(start_x),float(start_y)]).reshape(2,1)   #let us assume that we start in the middle of the front...
	positions.append(current_position)

	start_line = copy.deepcopy(current_position) #we deep copy cos current_position change...

	start_line[0]+=front_len #we add to the x val...

	rotation_angles = return_rotation_angles(numberOfSides)

	for rotation_angle in rotation_angles: #need to include the last one!
		rotation_matrix = rotation_matrix_2d(rotation_angle)
		span_vec = np.matmul(rotation_matrix,start_line)
		current_position=np.add(current_position,span_vec)
		positions.append(current_position)

	return positions

def rotation_matrix_from_axis_and_angle(axis_vector, angle):
	#axis_vector is unit vector, angle is in degrees
	u_x = axis_vector[0]
	u_y = axis_vector[1]
	u_z = axis_vector[2]

	col_1=[cos_d(angle) + (u_x**2)*(1-cos_d(angle))      , u_y*u_x*(1-cos_d(angle)) + u_z * sin_d(angle) , u_z*u_x*(1-cos_d(angle)) - u_y*sin_d(angle) ]
	col_2=[u_y*u_x*(1-cos_d(angle)) - u_z * sin_d(angle) , cos_d(angle) + (u_y**2)*(1-cos_d(angle))      , u_z*u_y*(1-cos_d(angle)) + u_x*sin_d(angle) ]
	col_3=[u_x*u_z*(1-cos_d(angle)) + u_y * sin_d(angle) , u_y*u_z*(1-cos_d(angle)) - u_x * sin_d(angle) , cos_d(angle) + (u_z**2)*(1-cos_d(angle))    ]

	out_mat = np.array([col_1, col_2, col_3])

	return out_mat



def return_rotation_angles(numberOfSides):
	TOTAL_DEGREES = 360
	rotation_angles=[]#leave as blank cos first duplicate front need to be rotated!
	for side in range(numberOfSides): #need to include the last one!
		rotation_angle = 360*(side/numberOfSides)
		rotation_angles.append(rotation_angle)

	return rotation_angles

def inverse_rotation_matrix(output_vec,input_vec):
	#calculates the inverse rotation matrix required to obtain output_vec from input_vec (IN 3D!!!!)
	input_vec = np.array(input_vec)
	output_vec = np.array(output_vec)

	v = np.cross(input_vec,output_vec)

	s = np.linalg.norm(v)

	c = np.dot(input_vec,output_vec)


	if c ==1:

		#you actually first neeed to check which axis reflected in....

		#we need to rotate around all 3 of them...
		rm1=np.identity(3)
		rm2=np.identity(3)
		rm3=np.identity(3)

		#now perform matrix multiplication
		cur_rot_mat = np.matmul(rm1,rm2)
		inverse_rotation_matrix = np.matmul(cur_rot_mat,rm3)

		return inverse_rotation_matrix

	elif c ==-1:

		v_skew_c1=[-1, 0, 0]
		v_skew_c2=[ 0, -1, 0]
		v_skew_c3=[ 0, 0, 1]

		v_skew = np.array([v_skew_c1, v_skew_c2, v_skew_c3])
		
		inverse_rotation_matrix = v_skew

		return inverse_rotation_matrix

	else:
		#c not equal to 1 so we have to do further calculations
		v_skew_c1=[    0, -v[2],  v[1]]
		v_skew_c2=[ v[2],     0, -v[0]]
		v_skew_c3=[-v[1],  v[0],     0]

		v_skew = np.array([v_skew_c1, v_skew_c2, v_skew_c3])

		inv_rot_term1 = np.identity(3)
		inv_rot_term2 = v_skew
		inv_rot_term3 = np.matmul(v_skew,v_skew)*(1/(1+c))

		inverse_rotation_matrix = np.add(inv_rot_term1,inv_rot_term2)
		inverse_rotation_matrix = np.add(inverse_rotation_matrix,inv_rot_term3)

		return inverse_rotation_matrix

def return_default_normal_vectors():
	#returns 6 default normal vectors, to aid with set up
	normal_vectors = []
	norm_vals = [-1,1]
	for k in range(3):
		for nv in norm_vals:
			dir_vec = [0]*3
			dir_vec[k] = nv
			normal_vectors.append(normal_vec(dir_vec, len(normal_vectors)))
	
	return normal_vectors

def return_four_front_face_vertices(dims):
	out_verts = []
	#can rearrange make sure ordered correctly?
	for d in dims:
		out_verts.append(vertex(0,d[0],d[1],len(out_verts)+1,'FRONT',-1))
	return out_verts

def return_face(verts,normal_vec_index, normal_vector_vec):
	#takes a 4-ordered list of vectors and returns a face
	vert_indices = [ vert.index for vert in verts]

	vert_coordinates = [vertex.vert for vertex in verts]

	out_face = face(vert_indices,normal_vec_index, normal_vector_vec, vert_coordinates)

	return out_face

def return_plane_equation(input_vert_coordinates):

	#just select first three....

	if len(input_vert_coordinates)<3:
		print('error we cant define a 3d plane equation for less than 3 points!!')
		return False

	x1, y1, z1 = input_vert_coordinates[0]
	x2, y2, z2 = input_vert_coordinates[1]
	x3, y3, z3 = input_vert_coordinates[2]

	v1 = [x3 - x1, y3 - y1, z3 - z1]
	v2 = [x2 - x1, y2 - y1, z2 - z1]

	cp = [v1[1] * v2[2] - v1[2] * v2[1],
	  	  v1[2] * v2[0] - v1[0] * v2[2],
	  	  v1[0] * v2[1] - v1[1] * v2[0]]

	a, b, c = cp
	
	def plane_equation(input_vec):
		return a * input_vec[0] + b * input_vec[1] + c * input_vec[2]

	const_val = plane_equation([x1,y1,z1])

	return plane_equation, const_val

def return_plane_coefficients(plane_eq):
	x_gradient = plane_eq([1,0,0])
	y_gradient = plane_eq([0,1,0])
	z_gradient = plane_eq([0,0,1])

	return [x_gradient, y_gradient, z_gradient]



#interdependencies
from 	vertex 			import *
from 	normal_vec 		import *
from 	face 			import *
from 	rectangle_face 	import *
from 	front 			import *
from 	cut_hole   		import *
from 	feature 		import *
from 	building 		import *

