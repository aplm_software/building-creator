
from reqs import *

class feature:

	def __init__(self,FEATURE_FN):

		in_file = open(FEATURE_FN,'r+')
		lines = in_file.readlines()
		in_file.close()


		self.lines=lines
		self.joined_areas_found = False #cos we don't want to do this one twice!

		#collects the vn,vert,face as STRINGS from input file
		self.collect_verts(lines)
		self.collect_vns(lines)
		self.collect_faces(lines)

		self.return_x_y_z() #returns lists self.xs, self.ys, self.zs
		
		#now set up!
		self.set_up()

	def set_up(self):
		self.instantiate_normal_vectors() #we need to instantiate vn's first
		print('instantiated vns (step 1 of 7)')
		self.instantiate_vertices()
		print('instantiated vertices (step 2 of 7)')
		self.instantiate_faces()
		print('instantiated faces (step 3 of 7)')
		self.return_min_max() #Now we have new min, max
		print('returned min max (step 4 of 7)')

		self.find_joined_face_areas() #doesn't return correct vertices...
		print('found joined faces areas (step 5 of 7)')
		#self.simplify_faces() #need to also simplify faces
		#self.find_index_of_largest_face() # find largest face's index

		self.return_aspects_of_largest_faces() #return this largest one...
		print('returned indices of largest faces (step 6 of 7)')

		self.rotate_feature_to_default_norm() #need to find the front normal vector first!

		self.joined_areas_found=False
		self.find_joined_face_areas()
		self.return_aspects_of_largest_faces() #return this largest one...#now do this again!

		#return min_min_v_i etc
		self.return_vertices_for_bounding_points_default_norm() #neeed to do but hack for now

		#and also return the lengths etc....

		self.return_feature_lengths()

		#self.scale_feature(0.1)
		self.populate_output('test_feat.obj') #make acopy of it for meeee...mow
		print('populated output (step 7 of 7)')



		return self


	def return_extreme_points(self):
		self.min_min_v = self.vertices[self.min_min_v_i-1]
		self.min_max_v = self.vertices[self.min_max_v_i-1]
		self.max_max_v = self.vertices[self.max_max_v_i-1]
		self.max_min_v = self.vertices[self.max_min_v_i-1]


		return self


	def return_feature_lengths(self):
		self.return_min_max_v()
		self.horiz_length = self.max_y - self.min_y
		self.vert_length = self.max_z - self.min_z
		self.depth = self.max_x - self.min_x

		return self

	def rotate_feature_to_default_norm(self):
		#now instantiate the default orthonormal vectors!!!		
		self.orthonormal_vec_vertical = np.array([0,0,1])
		self.orthonormal_vec_horizontal = np.array([0,1,0])
		#print('current norm vec is: ')
		#print(self.feature_normal_vector.vec)
		#print('we are now rotating to this: ')
		#print(DEFAULT_NORMAL_VEC)
		default_n_inv_rot = inverse_rotation_matrix(DEFAULT_NORMAL_VEC,self.feature_normal_vector.vec)

		#self.populate_output('feature_pre_rotate.obj')
		#print('default n is: ')
		#print(default_n_inv_rot)
		self.rotate_feature_3dmat(default_n_inv_rot)

		self.rotate_feature_normal_vectors_3dmat(default_n_inv_rot)
		#self.populate_output('feature_post_rotate.obj')

		return self

	def rotate_feature_to_front_norm(self,front_norm):
		#print(front_norm)

		#print(self.feature_normal_vector.vec)
		default_n_inv_rot = inverse_rotation_matrix(self.highest_fs_normal_vec,front_norm)
		

		self.rotate_feature_3dmat(default_n_inv_rot)
		self.rotate_feature_normal_vectors_3dmat(default_n_inv_rot)
		return self

	def collect_verts(self,lines):
		self.vertex_strings=[]
		for l in lines:
			if l[0]=="v" and l[1] not in ["t","n"]:
				self.vertex_strings.append(l)

		return self

	def collect_vns(self,lines):
		self.normal_vector_strings=[]
		for l in lines:
			if l[0:2]=="vn":
				self.normal_vector_strings.append(l)

		return self

	def collect_faces(self,lines):
		self.face_strings=[]	
		for l in lines:
			if l[0]=="f":
				self.face_strings.append(l)
		return self


	def set_vertex_parent_index(self, parent_index):
		for v in self.vertices:
			v.parent_index = parent_index
		return self

	def add_vertex(self,vertex):
		self.vertices.append(vertex)
		return self

	def instantiate_vertices(self):

		self.vertices=[]

		for l in self.vertex_strings:
			split_str = reg_split(l)
			xval=float(split_str[1]) #xval at index 1
			yval=float(split_str[2]) #yval at index 2
			zval=float(split_str[3]) #zval at index 3

			cur_vert = vertex(xval, yval, zval,len(self.vertices)+ 1,'FEATURE',-1) #-1 indicates not set yet

			self.add_vertex(cur_vert)

		return self

	def instantiate_faces(self):
		self.faces=[]

		for l in self.face_strings:
			l = re.sub('f','',l)
			l = re.sub(' +',' ',l)
			l = re.sub('\t',' ',l)
			l = re.sub('\n','',l)

			#do some stuff to these lines....
			rep_str = re.findall('/[0-9]/',l) #returns the strings that we find...

			for rs in rep_str:
				l = l.replace(rs,',')
			#now we have split by the vertex and the normal...fmle and oy vey!

			#also replace the double slash!!!
			rep_str = re.findall('//',l) #returns the strings that we find...

			for rs in rep_str:
				l = l.replace(rs,',')
			#now we have split by the vertex and the normal...fmle and oy vey!
			#print('here is the resultant face string we deal w')
			#print(l)
			l = l.split(' ')
			vs = [l0 for l0 in l[1:]]

			vertices = [int(l0.split(',')[0]) for l0 in l[1:]]
			normals  = [int(l0.split(',')[1]) for l0 in l[1:]]


			#print('here are vertex indices')
			#print(vertices)
			
			face_vertex_coordinates = []
			#print('and here are vertex coordinates')
			#for v_i in vertices:
			#	print(self.vertices[v_i-1].get_vertex_vector())		

			if len(list(set(normals)))>1:
				print('warning at least one face of the feature corresponds to more than 1 normal vector...')

			normals = normals[0]
			#normals should all be the same!!!

			face_vertex_coordinates = []
			for v_i in vertices:
				face_vertex_coordinates.append(self.vertices[v_i-1].get_vertex_vector())
			face_normal_vector_vec = self.normal_vectors[normals-1].vec
			

			#print('here normals')
			#print(normals)

			#print('here face normal vector vec')
			#print(face_normal_vector_vec)


			new_f=face(vertices, normals, face_normal_vector_vec, face_vertex_coordinates)
			self.faces.append(new_f)		
		#easye and donne
		return self


	def shift_all_vertex_indices_up(self,shift_amount):
		for v in self.vertices:
			v.shift_index_up(shift_amount)
		return self

	def shift_all_feature_vertex_references_up(self,shift_amount):
		for f in self.faces:
			f.shift_all_face_vertex_references_up(shift_amount)
		return self

	def instantiate_normal_vectors(self):
		self.normal_vectors=[]

		for l in self.normal_vector_strings:
			split_str = reg_split(l)
			xn = float(split_str[1])
			yn = float(split_str[2])
			zn = float(split_str[3])

			normal_vec_vec=[xn,yn,zn]

			self.normal_vectors.append(normal_vec(normal_vec_vec,len(self.normal_vectors)+1))
		return self

	def return_x_y_z(self):
		self.xs=[]
		self.ys=[]
		self.zs=[]

		for l in self.vertex_strings:
			l=re.sub(' +',' ',l)
			split_l=l.split(' ')
			#print(split_l)
			self.xs.append(float(split_l[1]))
			self.ys.append(float(split_l[2]))
			self.zs.append(float(split_l[3]))
		return self

	def return_min_max(self):

		self.min_x = min(self.xs)
		self.max_x = max(self.xs)
		self.min_y = min(self.ys)
		self.max_y = max(self.ys)
		self.min_z = min(self.zs)
		self.max_z = max(self.zs)

		return self


	def return_min_max_v(self):
		#need to do for first vetices....

		self.xs = [float(v.x) for v in self.vertices]
		self.ys = [float(v.y) for v in self.vertices]
		self.zs = [float(v.z) for v in self.vertices]


		self.min_x = min(self.xs)
		self.max_x = max(self.xs)
		self.min_y = min(self.ys)
		self.max_y = max(self.ys)
		self.min_z = min(self.zs)
		self.max_z = max(self.zs)

		return self


	def simplify_faces(self):
		#this is reflective of the types of deals we have to make!
		all_normals_len=[]
		new_faces_list = []
		faces_to_delete = []

		kkk = len(self.faces) #total length...need to iterate through this honga jonescore...

		#it's wrong becuase
		#you need to select based on
		#1. same normal vector
		#2. all at least connected....

		already_combined_f_index=[]

		for f_i in range(len(self.faces)):

			cur_face = self.faces[f_i]
			all_verts = []
			if f_i in already_combined_f_index:
				pass #we will skip in this instance because it's already been combined
			else:
				for n_i in range(f_i,len(self.faces)):
					next_face = self.faces[n_i]
					cur_normals = set(cur_face.normal_vec) #deduped
					#print('the currr')
					#print(len(cur_normals))
					all_normals_len.append(cur_normals)
					if cur_face.normal_vec==next_face.normal_vec and len(set(cur_face.face_verts).intersection(set(next_face.face_verts)))>0 and len(cur_normals)==1:
						#actually it's easy we can just use the normal vector....
						all_verts = list(set(cur_face.face_verts + next_face.face_verts + all_verts)) #duplicates removed
						faces_to_delete.append(next_face) #add this face so we can delete it later....
						already_combined_f_index.append(n_i) #append the index....so don't lose it....
						if cur_face not in faces_to_delete:
							faces_to_delete.append(cur_face) #so we don't have duplicates

				#now if we have that len(all_verts)>0 then we carry out the next stage:
				if len(all_verts)>0: #check it's not a list
					#if this is the case, we need to make new face!
					v_nms = [int(v) - 1 for v in all_verts]
					vs = [self.vertices[v_n] for v_n in v_nms]
					vs_xyz = [[vs.x,vs.y,vs.z] for vs in vs]

					#find out the normal vec's direction...
					cur_norm = self.vns[int(list(cur_face.normal_vec)[0])-1].vec
					#print('jac')
					#print(cur_norm)
					#Find out the index of non-zero
					ind_nz = [c_n!=0 for c_n in cur_norm]
					#remove the index corresponding to ind_nz == True
					necessary_index = ind_nz.index(True)

					to_keep = [0,1,2]

					to_keep.remove(necessary_index)

					rem_vs = [[vs[to_keep[0]],vs[to_keep[1]]] for vs in vs_xyz] #remaining ones!!

					#find midpoints....
					rem_v0 = [float(vs[0]) for vs in rem_vs]
					rem_v1 = [float(vs[1]) for vs in rem_vs]

					mp0 = (max(rem_v0)-min(rem_v0))/2
					mp1 = (max(rem_v1)-min(rem_v1))/2

					#now construct vectors....

					mpd_vs = [[float(vs[0])-mp0,float(vs[1]-mp1)] for vs in rem_vs] #so now they are normalised about 0,0

					starting_vertex = mpd_vs[0]
					all_angles = []	
					for extra_v in mpd_vs:
						#print('start and extrae')
						#print(starting_vertex)
						#print(extra_v)
						all_angles.append(v_angle_clockwise(starting_vertex,extra_v))

					angle_list = np.argsort(all_angles)
					#naoo need to export list of vertex indices
					v_export_list = [all_verts[al] for al in angle_list] #should be sorted now
					#face(v_export_list,list(cur_face.normal_vec)[0],self.vertices) #self.vertices will be the same...
					new_f=face(v_export_list,list(cur_face.normal_vec)[0],self.vertices) #self.vertices will be the same...

					new_faces_list.append(new_f)

		for face_cur in faces_to_delete:
			if face_cur in self.faces:
				self.faces.remove(face_cur)
		for face_cur in new_faces_list:
			self.faces.append(face_cur)

		return self

	def simplify_vertices(self):
		#we go through list of vertices
		#if current vertex matches any vertex that comes after it
		#then we replace references to the post-matched vertex with the pre-matched one in the faces
		#and then we delete the original vertex
		replaced_refs=[]


		orig_vertices=[]
		replacement_vertices=[]
		#set up default lists...
		for k in range(len(self.vertices)):
			orig_vertices.append(k)
			replacement_vertices.append(k)

		iter_list = []

		for k in range(len(self.vertices)):
			iter_list.append(k)

		for vertex_index in range(len(self.vertices)):
			cur_v=self.vertices[vertex_index].vert

			if vertex_index in iter_list:
				iter_list.remove(vertex_index) #delete it here...

			for next_v_index in iter_list:
				next_v=self.vertices[next_v_index].vert

				if cur_v == next_v and next_v_index not in replaced_refs: #check that replaced_refs doesn't already contain it!
					#need to replace it!	
					replacement_vertices[next_v_index]=vertex_index

					old_v = next_v_index + 1 #increment by 1
					new_v = vertex_index + 1 #increment by 1 
					#we have matched them
					replaced_refs.append(next_v_index)
					iter_list.remove(next_v_index)
					#go through faces list and replace references...
					for f_face in self.faces:
						if str(old_v) in f_face.face_verts: #if it's contained
							#update face_str
							#f_face.face_str = f_face.face_str.replace(' ' + str(old_v)+'//', ' ' + str(new_v)+'//')
							rep_index = f_face.face_verts.index(str(old_v)) #but the central idea here is that it can only be in there once!!!!
							#update vertices
							f_face.face_verts[rep_index]=str(new_v)

							f_face.form_face_str() #as we have replaced the old vertex references....
		#delete all them!!
		#reverse list first....
		replaced_refs.sort(reverse=True)
		for k in replaced_refs:
			del self.vertices[k]
		#find out all faces present still...

		#go through all faces and find which vertices still present....
		remaining_vertices=[]
		for cur_face in self.faces:
			for v in cur_face.face_verts:
				remaining_vertices.append(v)

		rv = list(set(remaining_vertices)) #rv is list of remaining vertices!!!!!
		rv = [int(r) for r in rv]
		rv.sort()
		#and now fix any refs as req'd
		for k in range(len(rv)):
			old_v=rv[k] 
			new_v=k+1 #increment by 1 for offset
			for f_face in self.faces:
				if str(old_v) in f_face.face_verts: #if it's contained
					#update face_str
					f_face.face_str = f_face.face_str.replace(' ' + str(old_v)+'//', ' ' + str(new_v)+'//')
					rep_index = f_face.face_verts.index(str(old_v))
					#update vertices
					f_face.face_verts[rep_index]=str(new_v)

		#so this is ok....
		#now change vertex index accordingly:
		for k in range(len(self.vertices)):
			self.vertices[k].index=k+1 # +1 because k is started at 0!!!
		#ok this means that it works fine here!!! fmle.
		return self
						
	def find_joined_face_areas(self):
		
		if self.joined_areas_found==True:
			return self
		else: 
			already_added=[]
			self.global_f_areas=[]
			self.global_f_faces=[]
			self.global_f_normal_vec=[]
			self.global_f_normal_vec_i=[]
			for cur_f_i in range(len(self.faces)):
				#cur_f_i start from 0, not 1
				current_f_adds = [cur_f_i]
				current_vert_indices = self.faces[cur_f_i].face_vertex_indices
				current_normal_vector_vec  = self.faces[cur_f_i].normal_vector_vec #check, can ONLY be a single (no list!)
				current_normal_vector_index = self.faces[cur_f_i].normal_vec_index #index device!
				current_v_vals=[]

				for cv_i in current_vert_indices:
					current_v_vals.append(self.vertices[cv_i-1].vert)

				cur_f_equation, cur_f_equation_const_val = return_plane_equation(current_v_vals)

				if cur_f_i in already_added:
					hello = 'hello'
				else:
					for next_f_i in range(cur_f_i+1,len(self.faces)): #doesn't include the final one:
						if next_f_i in already_added:
							hello = 'hello'
						else:
							next_vert_indices = self.faces[next_f_i].face_vertex_indices
							next_normal_vector_vec  = self.faces[next_f_i].normal_vector_vec

							next_v_vals=[]

							for nv_i in next_vert_indices:
								next_v_vals.append(self.vertices[nv_i-1].vert)

							next_f_equation, next_f_equation_const_val = return_plane_equation(next_v_vals)

							absolute_current_norm_vec = [abs(float(cv)) for cv in current_normal_vector_vec]
							absolute_next_norm_vec = [abs(float(nv)) for nv in next_normal_vector_vec]

							if absolute_current_norm_vec==absolute_next_norm_vec and len(set(current_vert_indices).intersection(set(next_vert_indices)))>0: 
								#normal vec is as req'd
								#and also at least one vertex is shared
								#create equations for them.....
								#if the equations are the same....
								#they can be considered to be part of the same face....
								#try 3 points for equations...current_v_vals will do!

								cur_v_eq_pts = [cur_f_equation(cur_v_val) for cur_v_val in current_v_vals]


								next_v_eq_pts = [next_f_equation(next_v_val) for next_v_val in next_v_vals]



								difference_eq = [abs(cur_v_eq_pts[i]-next_v_eq_pts[i]) for i in range(len(cur_v_eq_pts))] #must use absolute!

								if sum(difference_eq)<EPS_PLANE_CONTAIN:	#indicates that the value is the same cos eq = same
									current_f_adds.append(next_f_i)

					global_face_area=0
					for f_i in current_f_adds:
						global_face_area+=self.faces[f_i].surf_area

					#apppend to globals for later use :-)
					self.global_f_areas.append(global_face_area) # a record of the areas contained

					current_f_adds = [cf+1 for cf in current_f_adds] #Increment by 1 here...

					self.global_f_faces.append(current_f_adds) #a record of which face indices correspond to which areas STARTING FROM 1

					#print('current normal vec index is')
					#print(current_normal_vector_index)

					self.global_f_normal_vec.append(current_normal_vector_vec) #the vector itself ie [0,1,0]
					self.global_f_normal_vec_i.append(current_normal_vector_index) #the index

					for f_i in current_f_adds:
						self.faces[f_i-1].global_face_area=global_face_area

					for f_i in current_f_adds:
						already_added.append(f_i-1)

			#self.joined_areas_found=True #cos we have now found the joined face areas!

			#Because it's an index tho you neeeeeed to make sure the index starts from 1!!!
			return self

	def reset_highest_fs_normal_vec_norms_angle_rotation(self, rotation_angles):

		rotation_matrix = return_rotation_matrix_3d(rotation_angles)
		self.reset_highest_fs_normal_vec_norms_3dmat(rotation_matrix)

		return self	

	def reset_highest_fs_normal_vec_norms_3dmat(self, rotation_matrix):

		new_normal_vec = self.feature_normal_vector.rotate_3dmat(rotation_matrix)

		for normal_vector in self.normal_vectors:
			if normal_vector.is_same(new_normal_vec):
				self.feature_normal_vector=normal_vector

		return self

	def return_normals_for_largest_faces(self):
		self.reset_highest_fs_normal_vec_norms([0,0,0]) #no rotation
		return self

	def return_aspects_of_largest_faces(self):
		#sorted...
		li = np.argsort(self.global_f_areas) #sf is the list of face indices (start from 0)

		self.largest_f_index = li[len(li)-1:][0] #take last element which will be the biggest!

		#1. we need the f indices
		self.largest_f_indices = self.global_f_faces[self.largest_f_index]
		#2. we need the corresponding normal vector
		self.highest_fs_normal_vec = self.global_f_normal_vec[self.largest_f_index]	#will be in vec form ie [0,1,0]	
		#3. we need the corresponding area
		self.highest_fs_area = 	self.global_f_areas[self.largest_f_index]

		#now also set front normal vector..., we can do it here
		for normal_vector in self.normal_vectors:
			#print(normal_vector.vec)
			if normal_vector.is_same(self.highest_fs_normal_vec):
				self.feature_normal_vector = normal_vector

		#print('self feature normal vec is: ')
		#print(self.feature_normal_vector.vec)


		return self

		#define two orthonormal vectors and check parity!!

		#we rotated to default so we can reverse engineer this...

		#new orthonormal vertical is just orignal unit vector z

		#new orthonormal horizontal is just original unit vector y

	def return_vertices_for_bounding_points_default_norm(self):

		#find all relevant faces and store their relevant vertices
		all_vertices_in_largest = []

		for f_index in self.largest_f_indices:
			all_vertices_in_largest+=(self.faces[f_index-1].return_vertex_coordinates()) #now we have coordinatereer...boome


		#print('here are all vertices in largest w coordinates')
		#print(all_vertices_in_largest)
		largest_xs = [all_vertices_in_largest[v_i][0] for v_i in range(len(all_vertices_in_largest))]
		largest_ys = [all_vertices_in_largest[v_i][1] for v_i in range(len(all_vertices_in_largest))]
		largest_zs = [all_vertices_in_largest[v_i][2] for v_i in range(len(all_vertices_in_largest))]

		if len(set(largest_xs))!=1:
			print('errrror not rotated properly when finding the bounds for largest face!!!!!')

		x_val = largest_xs[0]

		self.largest_face_constant_axis_value = x_val
		self.largest_face_horizontal_midpoint = return_mp(largest_ys)
		self.largest_face_vertical_midpoint = return_mp(largest_zs)

		min_horizs = min(largest_ys)
		max_horizs = max(largest_ys)

		min_verts = min(largest_zs)
		max_verts = max(largest_zs)

		for v in self.vertices:
			if v.x==x_val and v.y==min_horizs and v.z==min_verts:
				self.min_min_v=v
				self.min_min_v_i=v.index
			if v.x==x_val and v.y==min_horizs and v.z==max_verts:
				self.min_max_v=v
				self.min_max_v_i=v.index
			if v.x==x_val and v.y==max_horizs and v.z==max_verts:
				self.max_max_v=v
				self.max_max_v_i=v.index
			if v.x==x_val and v.y==max_horizs and v.z==min_verts:
				self.max_min_v=v
				self.max_min_v_i=v.index								

		#actually this one depends ! on whether x_val is a max or a min...

		all_xs = [v.x for v in self.vertices]

		if x_val==max(all_xs):
			print('max obtain')
			for v in self.vertices:
				if v.x==x_val and v.y==min_horizs and v.z==min_verts:
					self.min_min_v=v
					self.min_min_v_i=v.index
				if v.x==x_val and v.y==min_horizs and v.z==max_verts:
					self.min_max_v=v
					self.min_max_v_i=v.index
				if v.x==x_val and v.y==max_horizs and v.z==max_verts:
					self.max_max_v=v
					self.max_max_v_i=v.index
				if v.x==x_val and v.y==max_horizs and v.z==min_verts:
					self.max_min_v=v
					self.max_min_v_i=v.index	

			return self

		elif x_val==min(all_xs):
			for v in self.vertices:
				if v.x==x_val and v.y==min_horizs and v.z==min_verts:
					self.max_min_v=v
					self.max_min_v_i=v.index
				if v.x==x_val and v.y==min_horizs and v.z==max_verts:
					self.max_max_v=v
					self.max_max_v_i=v.index
				if v.x==x_val and v.y==max_horizs and v.z==max_verts:
					self.min_max_v=v
					self.min_max_v_i=v.index
				if v.x==x_val and v.y==max_horizs and v.z==min_verts:
					self.min_min_v=v
					self.min_min_v_i=v.index	

			return self	

		else:
			print('error!!!! X VAL NOT MAX OR MIN AS REQ.')		

	def scale_feature_axis(self,scale_amt,ax_str):
		#return min and max for axis....
		#where ax_str is like "x" or "y" or "z" etc etc etc...
		axis_amts = []
		for v in self.vertices:
			axis_amts.append(float(v.ret_axis(ax_str)))


		#print(axis_amts)
		min_ax = min(axis_amts)
		max_ax = max(axis_amts)

		ax_mpoint = (max_ax - min_ax)/2+min_ax
		sc_ov_dif = max_ax - min_ax
		differentiation_amt = -min_ax
		final_rest_amt = -differentiation_amt

		#move to zero point....
		for v in self.vertices:
			v.change_axis_rel(ax_str,differentiation_amt) #move to center
		#now scale....
		for v in self.vertices:
			v.scale_axis(ax_str,float(scale_amt))
		#now move back to original point....
		for v in self.vertices:
			v.change_axis_rel(ax_str,final_rest_amt) #move away from center

		self.update_feature_faces_vertex_coordinates()

		return self

	def translate_feature(self,translations):
		self.translate(translations)
		return self

	def translate(self,translations):

		for v in self.vertices:
			v.change_all_axes_relative(translations)

		self.update_feature_faces_vertex_coordinates()
		return self


	def update_feature_faces_vertex_coordinates(self):
		#called any time that we modify something (like rotate, translate)
		for f in self.faces:
			current_f_coordinates=[]
			#make sure don't mess up the order! important!
			relevant_v_is = f.face_vertex_indices
			for relevant_v_i in relevant_v_is:
				current_f_coordinates.append(self.vertices[relevant_v_i-1].get_vertex_vector())

			f.update_vertex_coordinates(current_f_coordinates)

		return self



	def scale_feature(self,scale_amts):
		self.scale_feature_axis(scale_amts[0],'x') #x axis
		self.scale_feature_axis(scale_amts[1],'y') #y axis
		self.scale_feature_axis(scale_amts[2],'z') #z axis
		return self

	def populate_output(self,FN):
		#open the file firste
		out_lines = open(FN,'w+')
		#now write vn's...
		for vn in self.normal_vectors:
			out_lines.write(vn.vec_str + '\n')
		#now write vertices...
		for vert in self.vertices:
			out_lines.write(vert.vertex_str + '\n')
		#now write faces...
		for face in self.faces:
			out_lines.write(face.face_str + '\n')# + ' ' + str(face.surf_area)

		out_lines.close()


	def find_feature_mp(self):
		#finds the front midpoint....

		#return all vertices
		rel_verts = self.vertices

		#find mp of all 3 axes....
		v_x = [v.x for v in rel_verts]
		v_y = [v.y for v in rel_verts]
		v_z = [v.z for v in rel_verts]

		mp_x=return_mp(v_x)
		mp_y=return_mp(v_y)
		mp_z=return_mp(v_z)

		#return the mid points!!
		self.mps =  [mp_x, mp_y, mp_z] 

		return self

	def center_feature(self):

		self.center_feature_vertices()

		return self



	def update_feature_faces_vertex_coordinates(self):
		#called any time that we modify something (like rotate, translate)
		for f in self.faces:
			current_f_coordinates=[]
			#make sure don't mess up the order! important!
			relevant_v_is = f.face_vertex_indices
			for relevant_v_i in relevant_v_is:
				current_f_coordinates.append(self.vertices[relevant_v_i-1].get_vertex_vector())

			f.update_vertex_coordinates(current_f_coordinates)

		return self


	def center_feature_vertices(self):
		#return all vertex indices associated with these faces

		self.find_feature_mp()

		change_amt = [-m for m in self.mps]

		for v in self.vertices:
			v.change_all_axes_relative(change_amt)

		self.update_feature_faces_vertex_coordinates()


		return self

	def rotate_feature_vertices_angles(self,rotation):
		for v in self.vertices:
			v.rotate_angles(rotation)
		return self

	def rotate_feature_vertices_3d_mat(self,rotation_matrix):
		for v in self.vertices:
			v.rotate_3d_matrix(rotation_matrix)
		return self

	def uncenter_feature_vertices(self):
		change_amt = [m for m in self.mps]

		for v in self.vertices:
			v.change_all_axes_relative(change_amt)

		return self


	def rotate_feature_3dmat(self,rotation_matrix):
		self.find_feature_mp()
		self.center_feature_vertices()
		self.rotate_feature_vertices_3d_matrix(rotation_matrix)




		self.uncenter_feature_vertices()
		self.update_feature_faces_vertex_coordinates()
		return self

	def reset_feature_orthonormal_vectors(self,rotation_matrix):

		print(self.orthonormal_vec_vertical)
		print(self.orthonormal_vec_horizontal)

		self.orthonormal_vec_vertical = np.matmul(rotation_matrix,self.orthonormal_vec_vertical)
		self.orthonormal_vec_horizontal = np.matmul(rotation_matrix,self.orthonormal_vec_horizontal)

		print(self.orthonormal_vec_vertical)
		print(self.orthonormal_vec_horizontal)

		return self


	def rotate_feature_normal_vectors_3dmat(self,rotation_matrix):
		self.recast_normal_vectors_3dmat(rotation_matrix) #we need to rotate all normal vectors by the same amount
		self.renumber_feature_vn_references_to_self()
		self.update_feature_normal_vector_after_rotation_3dmat(rotation_matrix) #updates rel_ax_ind...
		self.reset_highest_fs_normal_vec_norms_3dmat(rotation_matrix)
		self.reset_face_normal_vector_vectors()
		self.reset_feature_orthonormal_vectors(rotation_matrix)


		return self

	def reset_face_normal_vector_vectors(self):

		for f in self.faces:
			f.normal_vector_vec = self.normal_vectors[f.normal_vec_index-1].vec
		return self

	def update_feature_normal_vector_after_rotation_3dmat(self,rotation_matrix):

		old_feature_normal_vector = copy.deepcopy(self.feature_normal_vector)

		#print('old feature norm vec isee')
		#print(old_feature_normal_vector.vec)

		new_feature_normal_vector_vec = old_feature_normal_vector.rotate_3dmat(rotation_matrix) #returns a vector

		NEW_NORMAL_VEC_FOUND=False
		for normal_vector in self.normal_vectors:
			if normal_vector.is_same(new_feature_normal_vector_vec):
				NEW_NORMAL_VEC_FOUND=True
				self.feature_normal_vector = normal_vector
				#print('new feature norm vec isee')
				#print(new_feature_normal_vector_vec)



				#print('normal vec in question is: ')
				#print(normal_vector.vec)				


				return self

		if NEW_NORMAL_VEC_FOUND==False:
			print('errror no new feature_normal_vector found!!!')

		return self



		


	def rotate_feature_angles(self,rotation_angles):

		rotation_matrix = return_rotation_matrix_3d(rotation_angles)

		self.rotate_feature_3dmat(rotation_matrix)

		return self


	def recast_normal_vectors_3dmat(self,rotation_matrix):

		new_vn_vecs = []
		for vn in self.normal_vectors:
			new_vn_vecs.append(vn.rotate_3dmat(rotation_matrix))

		vn_map_additions=[]
		for new_vn_vec in new_vn_vecs:
			current_addition=None #null in this instance
			for feature_vn in self.normal_vectors:
				if feature_vn.is_same(new_vn_vec):
					current_addition=feature_vn.index

			if current_addition==None:
				#find index of new vector
				new_vn_index = length(self.normal_vectors)+1

				#create new vector
				self.normal_vectors.append(normal_vec(new_vn_vec, new_vn_index))

				#Might also need to update feature_vns!
				#remap this vn badboy honga!
				vn_map_additions.append(new_vn_index)

			else:

				vn_map_additions.append(current_addition) #which in this instance will be the feature_vn.index

		#print('vn map additions for the feature is: ')

		self.vn_map_additions = vn_map_additions #for use when we remap the faces

		#print(self.vn_map_additions)
		return self

	def recast_normal_vectors_angles_rotation(self,rotation_angles):

		rotation_matrix = return_rotation_matrix_3d(rotation_angles)
		self.recast_normal_vectors_3dmat(rotation_matrix)
		return self


	def renumber_feature_vn_references_to_self(self):
		for f in self.faces:
			f.update_vn_ref(self.vn_map_additions)
		self.vn_map_additions=[] #clear out! So we don't iterate twice!

		return self

	def renumber_feature_vn_references_to_front(self,mappings_from_to):
		for f in self.faces:
			f.update_vn_ref(mappings_from_to)
		return self

	def rotate_feature_to_vector(self,destination_vn_index,out_fn):

		input_vn_vec = self.vns[self.highest_fs_vn_ind-1].vec
		output_vn_vec= self.vns[destination_vn_index-1].vec

		inv_r_m = inverse_rotation_matrix(input_vn_vec,output_vn_vec)

		self.inv_r_m = inv_r_m

		self.rotate_feature_3d_matrix(inv_r_m,out_fn) #rotate feature 3d matrixe!!

		return self



	def rotate_feature_vertices_3d_matrix(self,rotation_matrix):
		for v in self.vertices:
			v.rotate_3d_matrix(rotation_matrix)
		return self


	def resize_relocate(self,l,const_val,WINDOW_SCALING_FACTOR):

		#self.return_indices_of_largest_faces() #just for checks 

		l_min_horiz = l[0]
		l_min_vert =  l[1]
		l_max_horiz = l[2]
		l_max_vert =  l[3]

		relevant_axes = [0,1,2].remove(self.rel_ax_ind)

		#print(self.rel_ax_ind)

		self.mins = [self.min_x,self.min_y,self.min_z]
		self.maxs = [self.max_x,self.max_y,self.max_z]

		del self.mins[self.rel_ax_ind]
		del self.maxs[self.rel_ax_ind]

		horiz_int = self.maxs[0] - self.mins[0]
		vert_int = self.maxs[1] - self.mins[1]

		l_horiz_int = l_max_horiz - l_min_horiz

		l_vert_int = l_max_vert - l_min_vert

		horiz_scale = float(l_horiz_int)/float(horiz_int)
		vert_scale = float(l_vert_int)/float(vert_int)

		#first relocate.....

		if self.rel_ax_ind==0:
			#necessary atm...

			#first move
			#horiz
			moving_distance_y = self.mins[0]-self.min_y
			for v in self.vertices:
				v.change_y_rel(moving_distance_y)


			#vert
			moving_distance_z = self.mins[1]-self.min_z
			for v in self.vertices:
				v.change_z_rel(moving_distance_z)

			#then scale....

			#scale horizontally...
			moving_distance_y = self.mins[0]-self.min_y
			for v in self.vertices:
				moving_distance_horiz_scale = (float(v.y)-self.mins[0])/(horiz_int) #so now we're at 0, theoretically...
				moving_distance_horiz = l_min_horiz + moving_distance_horiz_scale * l_horiz_int

				v.update_ax_str('y',moving_distance_horiz)

			#scale vertically....
			moving_distance_z = self.mins[1]-self.min_z
			for v in self.vertices:
				moving_distance_vert_scale = (float(v.z)-self.mins[1])/(vert_int) #so now we're at 0, theoretically...
				moving_distance_vert = l_min_vert + moving_distance_vert_scale * l_vert_int

				v.update_ax_str('z',moving_distance_vert)
				
			#scale in th'other....
			#actually just assume that it's at 0....
			scaling_amt = min(horiz_scale, vert_scale)

			for v in self.vertices:
				x_scale_amt = (float(v.x))/(self.max_x-self.min_x)*scaling_amt
				#print('scaling amt is!')
				#print(x_scale_amt)
				v.update_ax_str('x',float(v.x)*0.1) #+ float(v.x)*x_scale_amt*WINDOW_SCALING_FACTOR)

			#now the world is oyster etc.....

			#now translate

			#refind min max etc
			self.return_min_max_v()

			#and then translate....
			for v in self.vertices:
				v.change_x_rel(const_val-self.max_x)

			return self

		if self.rel_ax_ind==1:
			#necessary atm...

			#first move			
			moving_distance_x = self.mins[0]-self.min_x
			for v in self.vertices:
				v.change_x_rel(moving_distance_x)

			for v in self.vertices:
				v.change_z_rel(moving_distance_z)

			#then scale....

			#scale last ax...

		if self.rel_ax_ind==2:
			print('warrrrning rel ax 2 triggered!')
			#necessary atm...

			#first move			
			moving_distance_x = self.mins[0]-self.min_x
			for v in self.vertices:
				v.change_x_rel(moving_distance_x)

			moving_distance_y = self.mins[1]-self.min_y
			for v in self.vertices:
				v.change_y_rel(moving_distance_y)
