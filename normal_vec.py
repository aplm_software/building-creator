from reqs import *

EPS_ROTATION=0.01 #rotation EPSILON....if it's within these ranges we'll consider it to be the same as another vn...

class normal_vec:
	def __init__(self,dir_vec,index):
		
		self.vec = [float(dir_vec[0]),
					float(dir_vec[1]),
					float(dir_vec[2])]

		if self.vec[0]==0 and self.vec[1]==0:
			self.rel_ax_ind = 2
		if self.vec[0]==0 and self.vec[2]==0:
			self.rel_ax_ind = 1
		if self.vec[1]==0 and self.vec[2]==0:
			self.rel_ax_ind = 0

		self.index=index

		self.form_vec_str() #form the string used for output purpose!!

	def return_rel_ax_ind(self):
		return self.rel_ax_ind

	def form_vec_str(self):
		self.vec_str = 'vn ' + '{:<08f}'.format(self.vec[0]) + ' ' + '{:<08f}'.format(self.vec[1]) + ' ' + '{:<08f}'.format(self.vec[2])
		return self

	def is_same(self,comparison_vec):
		#checks to see if current nv and rotat v are the same within a bound of EPS_ROTATION...
		x_same = False
		y_same = False
		z_same = False

		if self.vec[0]>comparison_vec[0]-EPS_ROTATION and self.vec[0]<comparison_vec[0]+EPS_ROTATION:
			x_same = True
		if self.vec[1]>comparison_vec[1]-EPS_ROTATION and self.vec[1]<comparison_vec[1]+EPS_ROTATION:
			y_same = True
		if self.vec[2]>comparison_vec[2]-EPS_ROTATION and self.vec[2]<comparison_vec[2]+EPS_ROTATION:
			z_same = True

		if x_same and y_same and z_same:
			return True
		else:
			return False

	def rotate_vec(self,rotation):
		rotation_matrix = return_rotation_matrix_3d(rotation)
		self.rotate_3dmat(rotation_matrix)
		return self

	def rotate_3dmat(self,rotation_matrix):
		cur_v = np.array(self.vec)
		out_v = np.matmul(rotation_matrix,cur_v)
		return out_v


