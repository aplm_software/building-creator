from reqs import *


#reset the 'cumulative' directory...

all_files = os.listdir('.')

if 'cumulative' in all_files:

	#first remove all the files in this folder...

	for cumulative_file in os.listdir('./cumulative'):
		os.remove('./cumulative/'+cumulative_file)
	#now remove the folder!
	os.rmdir('./cumulative')

#now make the folder again (potential overkill...but workflow process important)

os.makedirs('cumulative')

dims = [[0,0],[0,10],[1,10],[1,0]]

b1 = building()

b1.add_blank_front(dims)
b1.add_blank_front(dims)
b1.add_blank_front(dims)
b1.add_blank_front(dims)

print('here is f1 front normal')
print(b1.fronts[0].front_normal_vector.vec)
print('here is f2 front normal')
print(b1.fronts[1].front_normal_vector.vec)

print('here is f3 front normal')
print(b1.fronts[2].front_normal_vector.vec)


b1.rotate_front_rotation_angles(2,[0,0,90])
#b1.fronts[1].populate_output('FRONT_2.obj')
b1.fronts[1].translate_front([0.5,-0.5,0])

b1.rotate_front_rotation_angles(3,[0,0,180])
#b1.fronts[1].populate_output('FRONT_3.obj')
b1.fronts[2].translate_front([1,0,0])


b1.rotate_front_rotation_angles(4,[0,0,270])
#b1.fronts[1].populate_output('FRONT_4.obj')
b1.fronts[3].translate_front([0.5,0.5,0])

#all's good until here!

b1.add_feature_to_building('window1.obj')

#b1.features[0].populate_output('WINDOW1_post_ad.obj')

#all's good until here!

def return_square_points(start, height, width):
	#returns square for appending features

	pt1 = start
	pt2 = [start[0],start[1]+height]
	pt3 = [start[0]+width, start[1]+height]
	pt4 = [start[0]+width, start[1]]

	return [pt1,pt2,pt3,pt4]

NUM_VERTS = 10
NUM_HORIZ = 20
EPS_HEIGHT_BOUNDING = 0.01
EPS_WIDTH_BOUNDING = 0.01


for FRONT_INDEX in range(4):
	b1.fronts[FRONT_INDEX].return_front_height_width()

	height = b1.fronts[FRONT_INDEX].height
	width = b1.fronts[FRONT_INDEX].width
	height_spacing = (height-2*EPS_HEIGHT_BOUNDING)/NUM_VERTS #between features b1.fronts[FRONT_NUM].height
	width_spacing =  (width-2*EPS_WIDTH_BOUNDING)/NUM_HORIZ #between features  b1.fronts[FRONT_NUM].width

	total=1
	for num_vert in range(1,NUM_VERTS+1):
		for num_horiz in range(1,NUM_HORIZ+1):
			current_start_pt= [EPS_WIDTH_BOUNDING+(num_horiz-1)*width_spacing, 	EPS_HEIGHT_BOUNDING+(num_vert-1)*height_spacing]

			current_vert_pt = height_spacing

			current_horiz_pt =width_spacing

			locations = return_square_points(current_start_pt,current_vert_pt,current_horiz_pt)

			#print('locations is ')
			#print(locations)

			b1.fronts[FRONT_INDEX].add_feature_to_front(b1.features[0],locations)
			b1.fronts[FRONT_INDEX].populate_output('cumulative/front'+ str(FRONT_INDEX) + '_INDEX_' + str(total) + '.obj')

			print('total done for this front: ' + str(FRONT_INDEX))
			print(total)
			total+=1

#indexing is off!
#locations = return_square_points([0.1,0.1],0.5,0.3)



#b1.fronts[1].add_feature_to_front(b1.features[0],locations)
#b1.fronts[2].add_feature_to_front(b1.features[0],locations)
#b1.fronts[3].add_feature_to_front(b1.features[0],locations)



b1.populate_output('TESTING_NEW_FRONT.OBJ')