#req import

import cv2
import numpy as np
import os
import math
import copy

#@author Archer Moore
#@date 26-11-2018

#Function Definitions


#distance between two arbitrary pts (not based on line class)
def dist_pt_1_2(pt1,pt2):
	x1,y1 = pt1
	x2,y2=  pt2
	xtot = x2-x1

	ytot = y2-y1
	dist = math.sqrt(xtot**2 + ytot**2)

	return dist

#polygon class
class polygon:
	#container class for coordinates list
	#will use to redraw shape outlines at end of process
	#not actually used yet..

	def __init__(self,coord1,coord2):
		self.coords=[]
		self.add_coord(coord1)
		self.add_coord(coord2)


	def add_coord(self,new_coord):
		#check if it already exist in there
		for coord in self.coords:
			if coord.is_same(new_coord):
				return False

		#if doesnt exist then add and return ^,^
		self.coords.append(new_coord)
		return self

	def return_coords(self):
		#we return the coordinates as floats
		#for drawing the shapes
		coords_list=[]
		for coord in self.coords:
			coords_list.append(coord.x)
			coords_list.append(coord.y)

		return coords_list

#coord class

class coord:

	def __init__(self, x, y, parent_line):
		self.x=x
		self.y=y
		self.parent_line = parent_line

	def is_same_coord(self,input_coord):
		if self.x==input_coord.x and self.y==input_coord.y:
			return True
		else:
			return False

	def is_same_pts(self,x_co,y_co):
		#input x and y as separate floats (not class object)
		if self.x==x_co and self.y==y_co:
			return True
		else:
			return False		

	def return_x_y(self):
		return [self.x,self.y]

	def distance_coord(self,comp_coord):
		#returns the distance between current and comparison coordinate
		dist = dist_pt_1_2(self.return_x_y(),comp_coord.return_x_y())

		return dist

	def __str__(self):
		#string format output...
		out_str = 'x: {:.2f}\ty: {:.5f}\tdist: {:.2f}'.format(self.x,self.y,self.parent_line.length)
		return out_str

class line:

	def __init__(self,dims):
		#we receive output from LSD detector in the form:

		#np.array([x1, y1, x2, y2])

		#transpose this into relevant variables

		self.x1, self.y1, self.x2, self.y2 = dims[0]

		#initialise length
		self.calc_length()

	def calc_length(self):
		self.xtot = self.x2-self.x1
		self.ytot = self.y2-self.y1
		self.length = math.sqrt(self.xtot**2 + self.ytot**2)
		return self

	def return_first_points(self):
		return [self.x1,self.y1]

	def return_second_points(self):
		return [self.x2,self.y2]

	def return_all_coordinates(self):

		return [self.x1,self.y1,self.x2,self.y2]


	def add_coord_objects(self,coords):
		#to be called after we have added new coords based on the fpt vals obtained!
		#a reference only...
		self.coords = coords
		return self

	def is_same(self,comp_line):
		#compares two lines and either returns T/F

		if self.return_all_coordinates == comp_line.return_all_coordinates:
			return True
		else:
			return False

	def return_drawable_line(self):
		return np.array([self.x1,self.y1,self.x2,self.y2],dtype='float32')
#main class
class LSD_Image:

	def __init__(self,input_fn):

		self.polygons=[] #empty list for the moment
		self.coords=[]#empty list for the moment
		self.lines=[]#empty list for the moment


		self.input_fn =input_fn 

		self.output_blank_lines=None

		self.output_blank_polygons=None

		self.input_image=None


		self.load_img()

		self.create_grey_copy()
		self.create_blank_copy()

		#img w lines?

		#blank w lines?

		#img w poly?

		#blank w poly?

	def load_img(self):
		print('input is: ' + self.input_fn)
		self.orig_img = cv2.imread(self.input_fn)
		return self

	def create_grey_copy(self):
		#conv to grey
		try:
			self.grey_img = cv2.cvtColor(self.orig_img, cv2.COLOR_BGR2GRAY)

		except:
			print('Error no loaded img')
		finally:
			return self

	def create_blank_copy(self):
		try:
			self.g_height, self.g_width = self.grey_img.shape
			self.blank_img = np.zeros((self.g_height,self.g_width,3), np.uint8)
		except:
			print('Error when trying to access grey image for blank copy!')
		finally:
			return self

	def read_csv_lines(self):
		#Create default parametrization LSD
		self.lsd = cv2.createLineSegmentDetector(2) #use 2 as detection mode, anecdotally gives best results

		#Detect lines in img
		self.detected_lines = self.lsd.detect(self.grey_img)[0] #Position 0 of the returned tuple contains detected lines

		return self

	def convert_csv_output_to_coordinates_and_lines(self):
		#More coupling than would be desired here!
		#Review!

		#create new line objects
		for line_coords in self.detected_lines:
			self.lines.append(line(line_coords))

		for line_ in self.lines:
			fc = line_.return_first_points()
			sc = line_.return_second_points()
			coord_1 = coord(fc[0],fc[1], line_)
			coord_2 = coord(sc[0],sc[1], line_)

			#set in line obj itself

			self.coords.append(coord_1)
			self.coords.append(coord_2)


		#create bk
		self.lines_bk = copy.deepcopy(self.lines)
		self.coords_bk = copy.deepcopy(self.coords)

		return self

	def restore_bk_lsd_lines_coords(self):
		self.lines = self.lines_bk
		self.coords = self.coords_bk
		return self

	def delete_line_and_coords_by_index(self,line_index):
		self.delete_line_and_coords(self.lines[line_index])
		return self

	def delete_line_and_coords(self,line_):
		#find appropriate coords and delete those
		#then delete line
		#coords first

		c1 = line_.return_first_points()
		c2 = line_.return_second_points()

		del_coord_index = []
		for i, coord_ in enumerate(self.coords):
			if coord_.is_same_pts(c1[0],c1[1]) or coord_.is_same_pts(c2[0],c2[1]):
				del_coord_index.append(i)

		#sort and reverse indices...
		del_coord_index_sorted = sorted(del_coord_index,reverse=True)

		for coord_i in del_coord_index:
			del self.coords[coord_i]

		#do lines
		for i, line_o in enumerate(self.lines):
			if line_.is_same(line_o):
				line_del_index=i

		del self.lines[line_del_index]

		return self

	def filter_lines_to_threshold_length(self,threshold):

		THRESH=threshold
		height, width = self.grey_img.shape
		MIN_DIM = min(height, width)
		THRESH_DIST = MIN_DIM * THRESH

		del_ind = []

		#Get rid of lines smaller than some specified distance
		for i, line_  in enumerate(self.lines):
			if line_.length<THRESH_DIST:
				del_ind.append(i)

		del_ind_sorted = sorted(del_ind,reverse=True)

		for d_i in del_ind_sorted:
			self.delete_line_and_coords_by_index(d_i)

		return self

	def return_lines_for_drawing(self,lines):
		#returns coords as np object to be handled by cv2..

		out_lines = np.empty([0,1,4])

		for i, line_ in enumerate(lines):

			cur_coords = np.array([line_.return_all_coordinates()])

	
			out_lines = np.concatenate([out_lines,cur_coords],axis=None).reshape(-1,i+1,4)
		return out_lines[0]

	def modify_to_complete_polygon(self):
		#todo

		return self

	def append_shortest_lines(self,threshold):

		#attempts to join polygons by joining lines together

		THRESH=threshold
		height, width = self.grey_img.shape
		MIN_DIM = min(height, width)
		THRESH_DIST = MIN_DIM * THRESH

		self.shortest_lines = []

		for i,current_coord in enumerate(self.coords):

			other_coords = [self.coords[k] for k in range(len(self.coords)) if k!=i] 

			min_dist = max(self.g_height,self.g_width) #default limit 

			for comp_cd in other_coords:
				if current_coord.distance_coord(comp_cd)<min_dist:
					current_coord_candidate = comp_cd
					min_dist = current_coord.distance_coord(comp_cd)
					candidate_coords = np.array([comp_cd.return_x_y() + comp_cd.return_x_y()],dtype='float32')


			if min_dist<THRESH_DIST:
				#print(candidate_coords)
				#create new line..
				self.shortest_lines.append(line(candidate_coords))


		self.lines+=self.shortest_lines

		return self

	def draw_lines(self):
		#draw the lines on img
		lines_to_draw = self.return_lines_for_drawing(self.lines).reshape(-1,1,4)

		lines_to_draw = np.array(lines_to_draw,dtype='int') #need int m'jon'es'core

		self.orig_img_w_lines = self.lsd.drawSegments(self.orig_img,lines_to_draw)
		self.blank_img_w_lines = self.lsd.drawSegments(self.blank_img,lines_to_draw)
		self.grey_img_w_lines = self.lsd.drawSegments(self.grey_img,lines_to_draw)

		return self

	def output_drawn_img(self):

		cv2.imwrite("lsd_outputs/out_orig.jpg", self.orig_img_w_lines)
		cv2.imwrite("lsd_outputs/out_blank.jpg", self.blank_img_w_lines)
		cv2.imwrite("lsd_outputs/out_grey.jpg", self.grey_img_w_lines)

		return self

	def set_up_lines(self):
		self.read_csv_lines()
		self.convert_csv_output_to_coordinates_and_lines()
		return self

	def filter_thresh(self,thresh):
		self.restore_bk_lsd_lines_coords() #restore orig
		self.filter_lines_to_threshold_length(thresh)
		return self

	def poly_short_lines(self,thresh):
		self.filter_lines_to_threshold_length(thresh)
		return self

	def output(self):
		self.draw_lines()
		self.output_drawn_img()
		return self


#compiles


#now test...

#GLOBAL_THRESH=0.05

#im1 = LSD_Image('1.jpg')

#make CSV lines
#im1.read_csv_lines()

#convert to req format as class variables`
#im1.convert_csv_output_to_coordinates_and_lines()
#1.jpg
#kill off smaller lines
#im1.filter_lines_to_threshold_length(GLOBAL_THRESH)

#attempt to join polygon
#im1.append_shortest_lines(GLOBAL_THRESH)

#actually draw the lines on!
#im1.draw_lines()

#and save!
#im1.output_drawn_img()


#detect the mean and variance of intensity when u try out diff polygon??



