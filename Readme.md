#Instructions for running Make Building script


1. Make virtual env, activate and install reqs. In Linux terminal, type:

python3.6 -m venv myvenv
source myvenv/bin/activate
pip install -r requirements.txt


2. Run example program. In Linux terminal, type:

python driver.py

This will load a default 'window1.obj' file which is a simple rectangular window

At each iteration, the program adds another window onto the relevant front. I can't remember exactly, but there are 4 fronts in this particular building. I think you can modify it to have any other number of fronts...although I haven't tested it properly...try doing it with 8 - it looks evil.
