from reqs import *

class cut_hole:

	def __init__(self,v1,v2,v3,v4,norm_vec,norm_vec_axis): #convention is min_min, min_max, max_max, max_min

		self.vertices=[]
		self.vertices.append(v1)
		self.vertices.append(v2)
		self.vertices.append(v3)
		self.vertices.append(v4)

		self.v1=v1
		self.v2=v2
		self.v3=v3
		self.v4=v4

		remaining_axes = [0,1,2]
		remaining_axes.remove(norm_vec_axis)

		self.min_min=v1.return_normed_axes(norm_vec_axis)
		self.min_max=v2.return_normed_axes(norm_vec_axis)
		self.max_max=v3.return_normed_axes(norm_vec_axis)
		self.max_min=v4.return_normed_axes(norm_vec_axis)


		self.norm_vec=norm_vec
		self.norm_vec_axis=norm_vec_axis

		self.find_mp() #find the midpoint of this one!


	def find_mp(self):
		#we need to find midpoint to compare to feature later on !
		xs = [float(v.x) for v in self.vertices]
		ys = [float(v.y) for v in self.vertices]
		zs = [float(v.z) for v in self.vertices]

		#the rel ax ind will contain extreme points....

		self.max_x = max(xs)
		self.min_x = min(xs)
		self.max_y = max(ys)
		self.min_y = min(ys)
		self.max_z = max(zs)
		self.min_z = min(zs)

		self.mps = [get_mp(xs),
					get_mp(ys),
					get_mp(zs)]

		return self		


	def return_scalable_amts(self,rel_ax_ind):

		maxes = [self.max_x,self.max_y,self.max_z]
		mins =  [self.min_x,self.min_y,self.min_z]

		self.maxes=maxes
		self.mins=mins

		return self

	def intersect(self,other_hole):
		#if it intersects...
		#added equality constraint....
		if (self.min_min[0] >= other_hole.max_min[0] or
		    self.max_min[0] <= other_hole.min_min[0] or
		    self.min_max[1] <= other_hole.min_min[1] or
		    self.min_min[1] >= other_hole.min_max[1]):
		    return False

		else:
			'''print ('HOLE ALREADY CUT HERE!!!')
			print(self.min_min[0] >= other_hole.max_min[0])
			print(self.max_min[0] <= other_hole.min_min[0])
			print(self.min_max[1] <= other_hole.min_min[1])
			print(self.min_min[1] >= other_hole.min_max[1])
			print('indiv axes')
			print([self.min_min[0],other_hole.max_min[0]])
			print([self.max_min[0],other_hole.min_min[0]])
			print([self.min_max[1],other_hole.min_min[1]])
			print([self.min_min[1],other_hole.min_max[1]])
			print('normes')
			print(self.norm_vec_axis)
			print(other_hole.norm_vec_axis)'''
			return True